using System.Collections;
using System.Collections.Generic;
using PvB.KM.ScoreSystems;
using UnityEngine;

namespace PvB.KM.BurningBoat
{
    /// <summary>
    /// Behavior for the Boats trying to board the ship from the left
    /// </summary>
    public class BoardingBoatBehavior : MonoBehaviour
    {
        /// <summary>
        /// Distance the boat should gets spawned away from the player
        /// </summary>
        private const float DISTANCE_FROM_BOAT = -150f;

        /// <summary>
        /// Rigidbody attached to this boat
        /// </summary>
        [SerializeField]
        private Rigidbody rigidBody;
        /// <summary>
        /// Turn speed for trying to rotate to the player
        /// </summary>
        [SerializeField]
        private float turnSpeed;
        /// <summary>
        /// Movement speed for this boat
        /// </summary>
        [SerializeField]
        private float movementSpeed;
        /// <summary>
        /// how much in front of the player the boat can spawn
        /// </summary>
        [Header("Spawn Adjustments")]
        [SerializeField]
        private float adjustmentInfrontPlayer;
        /// <summary>
        /// how much behind of the player the boat can spawn
        /// </summary>
        [SerializeField]
        private float adjustmentBehindPlayer;
        /// <summary>
        /// The target for the boat
        /// </summary>
        private Transform Target;
        /// <summary>
        /// If the boat is in its getting destroyed animation
        /// </summary>
        private bool boatBeingDestroyed;
        /// <summary>
        /// ImpactFX is used to set the effect for when the projectile hits an enemy or the player
        /// </summary>
        [SerializeField]
        private GameObject ImpactFX;
        /// <summary>
        /// target for boarding boats after the game ends
        /// </summary>
        [SerializeField]
        private Vector3 secondaryTarget;
        /// <summary>
        /// desides wether to use the secondary target
        /// </summary>
        private bool useSecondaryTarget = false;

        /// <summary>
        /// Initialisation class used for setting the position and setting the target
        /// </summary>
        /// <param name="Target">The target for the boat</param>
        /// <returns>This BoardingBoatBehavior</returns>
        public BoardingBoatBehavior Init(Transform Target)
        {
            this.Target = Target;

            transform.eulerAngles = new Vector3(0, Random.Range(0, 180), 0);
            transform.position = new Vector3(Target.position.x + DISTANCE_FROM_BOAT, 0, Target.position.z + Random.Range(adjustmentInfrontPlayer, adjustmentBehindPlayer));
            return this;
        }

        /// <summary>
        /// Plays animation and destroys this boat
        /// </summary>
        public void DestroyBoat()
        {
            boatBeingDestroyed = true;
            ScoreSystem.Instance().UpdateJetskiScore();
            Instantiate(ImpactFX, transform.position + transform.up * 2, transform.rotation * Quaternion.Euler(90f, 0f, 0f));
            LeanTween.rotateX(gameObject, -89, 1f);
            LeanTween.moveLocalY(gameObject, -3f, 2f)
                .setOnComplete(() => Destroy(gameObject));
        }

        /// <summary>
        /// Returns the distance this boat is from the target
        /// </summary>
        /// <returns>The distance of this boat to the player target</returns>
        public float GetDistanceFromTarget() => (transform.position - Target.position).magnitude;

        private void Update()
        {
            if (boatBeingDestroyed)
                return;

            Vector3 relativePos = Target.position - transform.position;
            if (useSecondaryTarget)
                relativePos = secondaryTarget - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);

            Quaternion finalRotation = Quaternion.RotateTowards(transform.rotation, rotation, turnSpeed * Time.deltaTime);
            finalRotation.eulerAngles = new Vector3(0, finalRotation.eulerAngles.y, finalRotation.eulerAngles.z);
            transform.rotation = finalRotation;
            rigidBody.AddRelativeForce(new Vector3(0, 0, movementSpeed * Time.deltaTime));
        }

        public void SwitchToSecondaryTarget() => useSecondaryTarget = true;
    }
}
