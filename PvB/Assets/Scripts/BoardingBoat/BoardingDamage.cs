using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Brandon Ruigrok
namespace PvB.KM.BurningBoat.Damage
{
	/// <summary>
	/// Feedback: Boarding damage is voor nu misschien redundant. Kijk meer naar Projectile behaviour.
	/// Daar kunnen we een abstract function voor maken. In die player handler kunnen we een manier vinden om netjes die
	/// projectile eruit kunnen vissen. Daar kan het dan de damage van uit roepen, en destroyed hem dan ook zelf.
	/// </summary>
	public class BoardingDamage : MonoBehaviour
	{
		[SerializeField]
		private BoxCollider trigger;

		private bool isOverlapping;

		//Psuedo: if trigger enters player hitbox with the player tag -> enter 1.5 second delay and if the hitboxes still touch,
		//regulated by a boolean that disables itself if the playerhitbox and the trigger doesn't overlap with each other anymore.

		private void OnTriggerEnter(Collider other)
		{
			if(other.tag == "Player")
			{
				isOverlapping = true;
			}
		}

		private void OnTriggerExit()
		{
			isOverlapping = false;
		}

		void countDown()
		{

		}
	}
}