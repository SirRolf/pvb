using PvB.KM.Util;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.CustomCamera
{
	/// <summary>
	/// Behavior for the camera in this game
	/// </summary>
	public class CameraBehavior : MonoBehaviour
	{
		/// <summary>
		/// gameobject for the main camera
		/// </summary>
		[SerializeField]
		private GameObject camera;
		/// <summary>
		/// transform for the main camera
		/// </summary>
		[SerializeField]
		private Transform cameraTransform;

		/// <summary>
		/// Position of the camera
		/// </summary>
		private Vector3 cameraPosition;
		/// <summary>
		/// Rotation of the camera
		/// </summary>
		private Vector3 cameraRotation;

		/// <summary>
		/// Set's the standard camera positions and rotations
		/// </summary>
		public void SetCameraPositions()
		{
			cameraPosition = cameraTransform.position;
			cameraRotation = cameraTransform.rotation.eulerAngles;
		}

		/// <summary>
		/// Returns the camera position to its original
		/// </summary>
		/// <param name="axis">what axis to reset</param>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void ResetCameraPosition(Axis axis, float duration = 1f)
		{
			switch (axis)
			{
				case Axis.X:
					LeanTween.moveX(camera, cameraPosition.x, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Y:
					LeanTween.moveY(camera, cameraPosition.y, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Z:
					LeanTween.moveZ(camera, cameraPosition.z, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				default:
					return;
			}
		}

		/// <summary>
		/// reset all the positions
		/// </summary>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void ResetCameraPosition(float duration = 1f)
		{
			LeanTween.move(camera, cameraPosition, duration)
				.setEase(LeanTweenType.easeOutQuad);
		}

		/// <summary>
		/// resets the camera  rotatioin
		/// </summary>
		/// <param name="axis">what axis to rest</param>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void ResetCameraRotation(Axis axis, float duration = 1f)
		{
			switch (axis)
			{
				case Axis.X:
					LeanTween.rotateX(camera, cameraRotation.x, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Y:
					LeanTween.rotateY(camera, cameraRotation.y, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Z:
					LeanTween.rotateZ(camera, cameraRotation.z, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				default:
					return;
			}
		}

		/// <summary>
		/// resets all camera rotations
		/// </summary>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void ResetCameraRotation(float duration = 1f)
		{
			LeanTween.rotate(camera, cameraRotation, duration)
				.setEase(LeanTweenType.easeOutQuad);
		}

		/// <summary>
		/// Moves the camera
		/// </summary>
		/// <param name="axis">what axis to move</param>
		/// <param name="amount">what value to change it to</param>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void MoveCamera(Axis axis, float amount, float duration = 1f)
		{
			switch (axis)
			{
				case Axis.X:
					LeanTween.moveX(camera, cameraPosition.x + amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Y:
					LeanTween.moveY(camera, cameraPosition.y + amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Z:
					LeanTween.moveZ(camera, cameraPosition.z + amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Transforms the location of the camera
		/// </summary>
		/// <param name="Location">what to transform it to</param>
		public void TransformLocationCamera(Vector3 Location) => cameraTransform.position = Location;
		/// <summary>
		/// Transforms the rotation of the camera
		/// </summary>
		/// <param name="rotation">what to rotate it to</param>
		public void TransformRotationCamera(Quaternion rotation) => cameraTransform.rotation = rotation;

		/// <summary>
		/// rotates the camera
		/// </summary>
		/// <param name="axis">what axis to rotate</param>
		/// <param name="amount">what value to change it to</param>
		/// <param name="duration">The amount it takes to do the tween</param>
		public void RotateCamera(Axis axis, float amount, float duration = 1f)
		{
			switch (axis)
			{
				case Axis.X:
					LeanTween.rotateX(camera, amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Y:
					LeanTween.rotateY(camera, amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				case Axis.Z:
					LeanTween.rotateZ(camera, amount, duration)
						.setEase(LeanTweenType.easeOutQuad);
					break;
				default:
					break;
			}
		}
	}
}
