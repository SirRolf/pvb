using PvB.KM.DepartmentManagment;
using PvB.KM.Util;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.CrewManagment
{
	/// <summary>
	/// Class for keeping track of your crew
	/// </summary>
	public static class CrewMemberManager
	{
		/// <summary>
		/// how many places has already been filled by crewmembers
		/// </summary>
		public static int placesFilled;
		/// <summary>
		/// If the player is already placed
		/// </summary>
		public static bool PlayerPlaced;
		/// <summary>
		/// Gets called everytime the departments change
		/// </summary>
		public static Action<DepartmentType, DepartmentStatus> Refresh;

		/// <summary>
		/// Dictionary of all the departments and their status
		/// </summary>
		private static Dictionary<DepartmentType, DepartmentStatus> departments = new Dictionary<DepartmentType, DepartmentStatus>()
		{
			{DepartmentType.Cannon ,DepartmentStatus.Empty },
			{DepartmentType.MachineGun ,DepartmentStatus.Empty },
			{DepartmentType.WaterCannon ,DepartmentStatus.Empty },
			{DepartmentType.Radar ,DepartmentStatus.Empty },
			{DepartmentType.Krane ,DepartmentStatus.Empty }
		};

		/// <summary>
		/// Updates the department to a diffrent status
		/// </summary>
		/// <param name="_type">what department to change</param>
		/// <param name="_status">to what status to change it to</param>
		/// <returns> if it succseeded</returns>
		public static bool UpdateDepartment(DepartmentType _type, DepartmentStatus _status)
		{
			switch (_status)
			{
				case DepartmentStatus.Empty:
					if (departments[_type] == DepartmentStatus.CrewMember)
						placesFilled--;
					else if (departments[_type] == DepartmentStatus.Player)
						PlayerPlaced = false;
					departments[_type] = _status;
					if (Refresh != null)
						Refresh.Invoke(_type, _status);
					return true;
				case DepartmentStatus.CrewMember:
					if (placesFilled + 1 <= Consts.CREW_MEMBER_AMOUNT)
					{
						placesFilled++;
						departments[_type] = _status;
						if (Refresh != null)
							Refresh.Invoke(_type, _status);
						return true;
					}
					else
						return false;
				case DepartmentStatus.Player:
					if (!PlayerPlaced)
					{
						PlayerPlaced = true;
						departments[_type] = _status;
						if (Refresh != null)
							Refresh.Invoke(_type, _status);
						return true;
					}
					else
						return false;
				default:
					return false;
			}
		}

		public static void UnasignAllCrew()
		{
			//Should be a custom function not just a for loop but this is just for testing
			for (int i = 0; i < Enum.GetNames(typeof(DepartmentType)).Length; i++)
				UpdateDepartment((DepartmentType)i, DepartmentStatus.Empty);
		}

		/// <summary>
		/// Reverts the changes to player placed and places filled
		/// </summary>
		public static void ClearAllDepartments()
		{
			PlayerPlaced = false;
			placesFilled = 0;
		}

		/// <summary>
		/// Returns the requested department status
		/// </summary>
		/// <param name="_type">what department you want to receive</param>
		/// <returns>the department status</returns>
		public static DepartmentStatus GetDepartmentStatus(DepartmentType _type) => departments[_type];
	}
}