using PvB.KM.DepartmentManagment;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PvB.KM.CrewManagment
{
	/// <summary>
	/// Behaviour for the crew toggles
	/// </summary>
	public class CrewUIToggleBehavior : MonoBehaviour, IPointerClickHandler
	{
		/// <summary>
		/// Type for this toggle
		/// </summary>
		[SerializeField]
		private DepartmentType type;
		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private CanvasGroup inactive;
		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private CanvasGroup player;
		/// <summary>
		/// 
		/// </summary>
		[SerializeField]
		private CanvasGroup crew;

		/// <summary>
		/// If the toggle is on or off
		/// </summary>
		private bool isOn;

		/// <summary>
		/// On pointer click for when you press this object
		/// </summary>
		/// <param name="eventData">Event payload associated with pointer (mouse / touch) events</param>
		public void OnPointerClick(PointerEventData eventData) => Click(eventData.button);

		/// <summary>
		/// function that updates calls the update department and changes the images of this toggle
		/// </summary>
		/// <param name="button">The button that got pressed</param>
		public void Click(PointerEventData.InputButton button)
		{
			LeanTween.cancel(inactive.gameObject);
			LeanTween.cancel(player.gameObject);
			LeanTween.cancel(crew.gameObject);

			if (button == PointerEventData.InputButton.Right && !isOn)
			{
				if (CrewMemberManager.UpdateDepartment(type, DepartmentStatus.CrewMember))
				{
					isOn = true;
					LeanTween.alphaCanvas(inactive, 0f, .5f)
						.setEase(LeanTweenType.easeOutSine);
					LeanTween.alphaCanvas(crew, 1f, .5f)
						.setEase(LeanTweenType.easeOutSine);
				}
			}
			else if (button == PointerEventData.InputButton.Left && !isOn)
			{
				if (CrewMemberManager.UpdateDepartment(type, DepartmentStatus.Player))
				{
					isOn = true;
					LeanTween.alphaCanvas(inactive, 0f, .5f)
						.setEase(LeanTweenType.easeOutSine);
					LeanTween.alphaCanvas(player, 1f, .5f)
						.setEase(LeanTweenType.easeOutSine);
				}
			}
			else if (isOn)
			{
				if (CrewMemberManager.UpdateDepartment(type, DepartmentStatus.Empty))
				{
					LeanTween.alphaCanvas(player, 0f, .5f)
						.setEase(LeanTweenType.easeOutSine);
					LeanTween.alphaCanvas(crew, 0f, .5f)
						.setEase(LeanTweenType.easeOutSine);
					LeanTween.alphaCanvas(inactive, 1f, .5f)
						.setEase(LeanTweenType.easeOutSine);
					isOn = false;
				}
			}
		}
	}
}
