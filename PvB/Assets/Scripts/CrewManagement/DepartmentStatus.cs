namespace PvB.KM.CrewManagment
{
	/// <summary>
	/// Diffrent types of status for the departments
	/// </summary>
	public enum DepartmentStatus
	{
		Empty,
		CrewMember,
		Player
	}
}
