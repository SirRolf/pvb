using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// activates back particles when the game starts
/// </summary>
public class DeactivateParticles : MonoBehaviour
{
    /// <summary>
    /// reference to the back particles
    /// </summary>
    [SerializeField]
    private GameObject backParticles;
    /// <summary>
    /// activates backParticles
    /// </summary>
    public void Activate() => backParticles.SetActive(true);
}
