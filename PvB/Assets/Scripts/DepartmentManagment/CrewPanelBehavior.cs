using PvB.KM.CrewManagment;
using PvB.KM.DepartmentManagment;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.KM.CrewManager.DepartmentManagment
{
    /// <summary>
    /// Behavior for the crewpanel in the  game canvas
    /// </summary>
    public class CrewPanelBehavior : MonoBehaviour
    {
        /// <summary>
        /// Image for the player head
        /// </summary>
        [Header("Images")]
        [SerializeField]
        private Image playerImage;
        /// <summary>
        /// Image for the first crew head
        /// </summary>
        [SerializeField]
        private Image firstCrewImage;
        /// <summary>
        /// Image for the second crew head
        /// </summary>
        [SerializeField]
        private Image secondCrewImage;

        /// <summary>
        /// Sprite for when a head is in use
        /// </summary>
        [Header("sprites")]
        [SerializeField]
        private Sprite unselected;
        /// <summary>
        /// Head for when the there is a player left
        /// </summary>
        [SerializeField]
        private Sprite player;
        /// <summary>
        /// Head for when there is a crewmember left
        /// </summary>
        [SerializeField]
        private Sprite crew;

        private void OnEnable() => CrewMemberManager.Refresh += UpdateImages;
        private void OnDisable() => CrewMemberManager.Refresh -= UpdateImages;

        /// <summary>
        /// Updates the heads to whatever sprite fits the situation
        /// </summary>
        /// <param name="department">Not Used</param>
        /// <param name="status">Not Used</param>
        private void UpdateImages(DepartmentType department, DepartmentStatus status)
        {
            playerImage.sprite = CrewMemberManager.PlayerPlaced ? unselected : player;

            Sprite secondCrewSprite = crew;
            Sprite firstCrewSprite = crew;
            if (CrewMemberManager.placesFilled >= 1)
                secondCrewSprite = unselected;
            if (CrewMemberManager.placesFilled >= 2)
                firstCrewSprite = unselected;
            firstCrewImage.sprite = firstCrewSprite;
            secondCrewImage.sprite = secondCrewSprite;
        }
    }
}
