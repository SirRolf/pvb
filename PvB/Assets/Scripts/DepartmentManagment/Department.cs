using UnityEngine;
using PvB.KM.Upgrades;
using PvB.KM.DepartmentManagment;
using PvB.KM.CrewManagment;

namespace PvB.KM.CrewManager.DepartmentManagment
{
	/// <summary>
	/// Abstract class for all the departments
	/// </summary>
	public abstract class Department : MonoBehaviour
	{
		/// <summary>
		/// Helps with calling a function in another script
		/// </summary>
		[SerializeField]
		public DepartmentType departmentType;

		/// <summary>
		/// Gets called everytime you add a crewmember or player into this department.
		/// </summary>
		public abstract void Activate(DepartmentStatus status);

		/// <summary>
		/// Gets called everytime you remove a crewmember or player from this department.
		/// </summary>
		public abstract void Disable();
		/// <summary>
		/// Sets up the department for further use
		/// </summary>
		public abstract void Initialize();
		/// <summary>
		/// Cleans up the department
		/// </summary>
		public abstract void CleanUp();
		/// <summary>
		/// Upgrades department
		/// </summary>
		public abstract void Upgrade();
	}
}
