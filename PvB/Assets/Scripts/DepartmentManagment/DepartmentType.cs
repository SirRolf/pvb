using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.DepartmentManagment
{
	/// <summary>
	/// all diffrent department types
	/// </summary>
	public enum DepartmentType
	{
		Cannon,
		MachineGun,
		WaterCannon,
		Radar,
		Krane
	}
}

