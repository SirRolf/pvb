using PvB.KM.CrewManagment;
using PvB.KM.CustomCamera;
using PvB.KM.DepartmentManagment;
using PvB.KM.DepartmentManagment.Pointer;
using PvB.KM.Projectile;
using PvB.KM.Sound.Shooting;
using PvB.KM.Upgrades;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PvB.KM.CrewManager.DepartmentManagment.Departments
{
	/// <summary>
	/// Department class for the Cannon Department
	/// </summary>
	public class CannonDepartment : Department
	{
		/// <summary>
		/// Transform of the main cannon component
		/// </summary>
		[SerializeField]
		private Transform cannon;
		/// <summary>
		/// The transform from the boat
		/// </summary>
		[SerializeField]
		private Transform boat;
		/// <summary>
		/// The main camera
		/// </summary>
		[SerializeField]
		private Camera camera;
		/// <summary>
		/// The main camera behavior
		/// </summary>
		[SerializeField]
		private CameraBehavior cameraBehavior;
		/// <summary>
		/// Gameobject for the barrel
		/// </summary>
		[SerializeField]
		private GameObject barrelGameObject;
		/// <summary>
		/// Projectile prefab
		/// </summary>
		[SerializeField]
		private ProjectileBehaviour projectile;
		/// <summary>
		/// Projectile Mk2 prefab
		/// </summary>
		[SerializeField]
		private ProjectileBehaviour projectileMk2;
		/// <summary>
		/// Projectile Mk3 prefab
		/// </summary>
		[SerializeField]
		private ProjectileBehaviour projectileMk3;
		/// <summary>
		/// The distance the camera moves forward when activating the cannon with the player
		/// </summary>
		[SerializeField]
		private float cameraZOffset;
		/// <summary>
		/// By how much the Crew can miss
		/// </summary>
		[Header("Crew Adjustment")]
		[SerializeField]
		private float aimOffset;
		/// <summary>
		/// The delay in between shots by the crew
		/// </summary>
		[SerializeField]
		private float aiShootDelay;
		/// <summary>
		/// If the crew is trying to hover over the target
		/// </summary>
		private bool aiIsLockingOn;
		/// <summary>
		/// The delay in between shots by the player
		/// </summary>
		[Header("Player adjustment")]
		[SerializeField]
		private float shootDelay;
		/// <summary>
		/// Click or hold firing
		/// </summary>
		[SerializeField]
		private bool fullAuto;
		/// <summary>
		/// The rotation speed of the cannons
		/// </summary>
		[SerializeField]
		[Range(0.1f, 1.5f)]
		private float rotationSpeed;
		/// <summary>
		/// The pointer in the game
		/// </summary>
		[Header("Tutorial")]
		[SerializeField]
		private PointerBehavior pointer;
		/// <summary>
		/// Enum with marks
		/// </summary>
		private MarkTypes type = MarkTypes.Mk1;
		/// <summary>
		/// If you should do the player behavior in  the update 
		/// </summary>
		private bool shouldDoPlayerBehavior;
		/// <summary>
		/// If you should do the crew behavior in  the update 
		/// </summary>
		private bool shouldDoCrewBehavior;
		/// <summary>
		/// Timer used for the ShootDelay of the player
		/// </summary>
		private float timer;
		/// <summary>
		/// The standard z axis for the barrel
		/// </summary>
		private float barrelZ;
		/// <summary>
		/// If the game already started
		/// </summary>
		private bool startedUp;
		/// <summary>
		/// reference to piratemanager
		/// </summary>
		[SerializeField]
		private PirateManager pirateManager;
		/// <summary>
		/// Sound effect for the cannon and machinegun
		/// </summary>
		[SerializeField]
		private ShootingSound sound;
		/// <summary>
		/// To help with sound triggering
		/// </summary>
		private bool soundIsPlaying = false;
		/// <summary>
		/// Pause screen gameobject
		/// </summary>
		[SerializeField]
		private GameObject pauseScreen;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Initialize()
		{
			barrelZ = barrelGameObject.transform.localPosition.z;
			timer = shootDelay;
			startedUp = true;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void CleanUp()
		{
			return;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Activate(DepartmentStatus status)
		{
			aiIsLockingOn = false;
			shouldDoPlayerBehavior = status == DepartmentStatus.Player;
			shouldDoCrewBehavior = status == DepartmentStatus.CrewMember;
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.MoveCamera(Util.Axis.X, cannon.position.x);
				cameraBehavior.MoveCamera(Util.Axis.Z, cannon.position.z + cameraZOffset);
				timer = shootDelay;
			}
		}

		//Listens to changes done by the Activate and Disable function
		private void Update()
		{
			if (shouldDoCrewBehavior && !aiIsLockingOn)
				CrewBehavior();
			if (shouldDoPlayerBehavior)
				PlayerBehavior();
			// This is really dirty, didn't have enough time to make it look better. Emphasis on dirty.
			if (pauseScreen.activeInHierarchy == true)
			{
				sound.StopSound();
				soundIsPlaying = false;
			}
		}

		/// <summary>
		/// Behavior for the crew trying to shoot the cannon
		/// </summary>
		private void CrewBehavior()
		{
			//Calculating the angle
			Vector3 closestBoatLocation = pirateManager.GetClosestBoatPosition();
			if (closestBoatLocation == null)
				return;
			float angle = -(Mathf.Atan2(closestBoatLocation.z, closestBoatLocation.x) * Mathf.Rad2Deg);
			angle += Random.Range(-aimOffset, aimOffset);
			//Actual rotating and shooting
			LeanTween.rotateY(cannon.gameObject, angle, aiShootDelay)
				.setEase(LeanTweenType.easeOutSine)
				.setOnComplete(() =>
				{
					LeanTween.cancel(barrelGameObject);
					aiIsLockingOn = false;
					if (!fullAuto) //AI cannon sound
						sound.PlayOneShotSound();
					else if (fullAuto && !soundIsPlaying) //AI machine gun sound
					{
						sound.PlaySound();
						soundIsPlaying = true;
					}
					switch (type)
					{
						case MarkTypes.Mk1:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk2:
							Instantiate(projectileMk2, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk3:
							Instantiate(projectileMk3, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						default:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
					}
					LeanTween.moveLocalZ(barrelGameObject, barrelZ + .3f, .2f)
						.setEase(LeanTweenType.easeOutBack)
						.setOnComplete(() =>
						{
							LeanTween.moveLocalZ(barrelGameObject, barrelZ, .5f).setEase(LeanTweenType.easeInExpo);
						});
				});
			aiIsLockingOn = true;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Disable()
		{
			sound.StopSound();
			soundIsPlaying = false;
			LeanTween.cancel(cannon.gameObject);
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.ResetCameraPosition(Util.Axis.X);
				cameraBehavior.ResetCameraPosition(Util.Axis.Z);
				//cameraBehavior.ResetCameraPosition(Util.Axis.Y);
				//cameraBehavior.ResetCameraRotation(Util.Axis.X);
			}
			shouldDoPlayerBehavior = false;
			shouldDoCrewBehavior = false;
		}

		/// <summary>
		/// Upgrade function
		/// </summary>
		public override void Upgrade()
		{
			int currentType = (int)type + 1;
			type = (MarkTypes)currentType;
		}

		/// <summary>
		/// Behavior for the player trying to shoot the cannon
		/// </summary>
		private void PlayerBehavior()
		{
			//Timer update
			timer += Time.deltaTime;

			//Get the new angle the cannon should rotate towards
			Vector3 mousePos;
			if (startedUp)
				mousePos = Input.mousePosition;
			else
				mousePos = pointer.transform.position;
			Vector3 objectPos = camera.WorldToScreenPoint(cannon.position);
			mousePos.x = mousePos.x - objectPos.x;
			mousePos.y = mousePos.y - objectPos.y;
			float angle = -(Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg);
			//Restricting the rotation to 180 degrees
			if (angle > 0 && angle < 90)
				angle = 0;
			else if (angle > 90 && angle < 180)
				angle = 180;
			//The rotating
			LeanTween.cancel(cannon.gameObject);
			LeanTween.rotateY(cannon.gameObject, angle, rotationSpeed);

			//Shooting handler, full auto can hold left mouse button
			if (fullAuto)
			{
				if (Input.GetMouseButton(0) && timer > shootDelay)
				{
					if (!soundIsPlaying && pauseScreen.activeInHierarchy == false)
					{
						sound.PlaySound();
						soundIsPlaying = true;
					}
					LeanTween.cancel(barrelGameObject);
					switch (type)
					{
						case MarkTypes.Mk1:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk2:
							Instantiate(projectileMk2, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk3:
							Instantiate(projectileMk3, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						default:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
					}
					LeanTween.moveLocalZ(barrelGameObject, barrelZ - .3f, .2f)
						.setEase(LeanTweenType.easeOutBack)
						.setOnComplete(() =>
						{
							LeanTween.moveLocalZ(barrelGameObject, barrelZ, .5f).setEase(LeanTweenType.easeInExpo);
						});
					timer = 0;
				}
				else if (soundIsPlaying && timer > shootDelay)
				{
					sound.StopSound();
					soundIsPlaying = false;
				}
			}
			//Semi auto, click to shoot, holding doesn't work
			else
			{
				if (Input.GetMouseButtonDown(0) && timer > shootDelay)
				{
					sound.PlayOneShotSound();
					LeanTween.cancel(barrelGameObject);
					switch (type)
					{
						case MarkTypes.Mk1:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk2:
							Instantiate(projectileMk2, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						case MarkTypes.Mk3:
							Instantiate(projectileMk3, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
						default:
							Instantiate(projectile, cannon.position, cannon.rotation * Quaternion.Euler(0, 90, 0));
							break;
					}
					LeanTween.moveLocalZ(barrelGameObject, barrelZ + .3f, .2f)
						.setEase(LeanTweenType.easeOutBack)
						.setOnComplete(() =>
						{
							LeanTween.moveLocalZ(barrelGameObject, barrelZ, .5f).setEase(LeanTweenType.easeInExpo);
						});
					timer = 0;
				}
			}
		}
	}
}