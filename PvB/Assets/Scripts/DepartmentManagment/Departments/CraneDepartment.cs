using PvB.KM.CrewManagment;
using PvB.KM.PowerBox;
using PvB.KM.Powerups;
using PvB.KM.CustomCamera;
using PvB.KM.Util;
using PvB.KM.Sound.Shooting;
using PvB.KM.Upgrades;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace PvB.KM.CrewManager.DepartmentManagment.Departments
{
	/// <summary>
	/// Department class for the Crane Department
	/// </summary>
	public class CraneDepartment : Department
	{
		/// <summary>
		/// Sets the name for the transform action
		/// </summary>
		public Action<Transform, BoxBehaviour> ThrowHook;
		/// <summary>
		/// Makes a list for the availabe boxes
		/// </summary>
		[SerializeField]
		private List<BoxBehaviour> boxPrefabs;
		/// <summary>
		/// The main camera in the game
		/// </summary>
		[SerializeField]
		private Camera camera;
		/// <summary>
		/// The main camera behavior
		/// </summary>
		[SerializeField]
		private CameraBehavior cameraBehavior;
		/// <summary>
		/// X offset for the camera
		/// </summary>
		private const int X_OFFSET = 20;
		/// <summary>
		/// Sets the hook GameObject
		/// </summary>
		[SerializeField]
		private GameObject hook;
		/// <summary>
		/// Sets the transform for the CraneLocation
		/// </summary>
		[SerializeField]
		private Transform CraneLocation;
		/// <summary>
		/// Sets the spawnThreshold used to spawn in the boxes
		/// </summary>
		[SerializeField]
		private float spawnThreshold;
		/// <summary>
		/// Sets the minimum & maximum time for the cube spawn in mk1
		/// </summary>
		[SerializeField]
		private Vector2 mk1TimeRange;
		/// <summary>
		/// Sets the minimum & maximum time for the cube spawn in mk2
		/// </summary>
		[SerializeField]
		private Vector2 mk2TimeRange;
		/// <summary>
		/// Sets the minimum & maximum time for the cube spawn in mk3
		/// </summary>
		[SerializeField]
		private Vector2 mk3TimeRange;
		/// <summary>
		/// Final Vector 2 time line for the upgrade
		/// </summary>
		private Vector2 finalTimeRange;
		/// <summary>
		/// Mark upgrader
		/// </summary>
		private MarkTypes type;
		/// <summary>
		/// Checks if the player can use the Crane or not
		/// </summary>
		private bool shouldDoPlayerBehavior;
		/// <summary>
		/// Checks if the crew should controle the crane
		/// </summary>
		private bool shoulDoCrewBehavior;
		/// <summary>
		/// Makes a list for the available boxes to spawn
		/// </summary>
		private List<BoxBehaviour> boxes = new List<BoxBehaviour>();
		/// <summary>
		/// gets list of boxes
		/// </summary>
		/// <returns>list of all boxes</returns>
		public List<BoxBehaviour> Boxes => boxes;
		/// <summary>
		/// Sets the timer for when the boxes can spawn
		/// </summary>
		private float spawnTimer;
		/// <summary>
		/// The powerupmanager used for activating a powerup
		/// </summary>
		[SerializeField]
		private PowerUpManager powerUpManager;
		/// <summary>
		/// Sound effect for the Crane
		/// </summary>
		[SerializeField]
		private ShootingSound sound;
		/// <summary>
		/// Bool to prevent clipping of audio.
		/// </summary>
		private bool soundIsPlaying = false;
		/// If the update function should function
		/// </summary>
		private bool ShouldUpdate;

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Initialize()
		{
			ShouldUpdate = true;
			ThrowHook += OnThrowHook;
			finalTimeRange = mk1TimeRange;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void CleanUp()
		{
			ShouldUpdate = false;
			ThrowHook -= OnThrowHook;
		}
		/// <summary>
		///  Activate makes it possibe to activate the players control over the crane
		/// </summary>
		public override void Activate(DepartmentStatus status)
		{
			shouldDoPlayerBehavior = status == DepartmentStatus.Player;
			shoulDoCrewBehavior = status == DepartmentStatus.CrewMember;
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.MoveCamera(Axis.X, CraneLocation.position.x + X_OFFSET);
				cameraBehavior.MoveCamera(Axis.Z, CraneLocation.position.z);
				cameraBehavior.RotateCamera(Axis.Y, 90);
			}
		}
		/// <summary>
		/// Disable is used here to de activate the player controls over the crane
		/// </summary>
		public override void Disable()
		{
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.ResetCameraPosition(Axis.X);
				cameraBehavior.ResetCameraPosition(Axis.Z);
				cameraBehavior.ResetCameraRotation(Axis.Y);
			}
			sound.StopSound();
			shouldDoPlayerBehavior = false;
			shoulDoCrewBehavior = false;
		}

		/// <summary>
		/// Update is used here to spawn the boxes calling the SpawnBox function once the Spawnthreshold hits
		/// </summary>
		private void Update()
		{
			if (!ShouldUpdate)
				return;
			spawnTimer += Time.deltaTime;

			if (spawnTimer >= spawnThreshold)
			{
				spawnThreshold = UnityEngine.Random.Range(finalTimeRange.x, finalTimeRange.y);
				spawnTimer = 0;
				SpawnBox();
			}
			if (shoulDoCrewBehavior)
				if (boxes.Any())
					for (int i = 0; i < boxes.Count; i++)
						if (boxes[i].Progress > 45 && boxes[i].Progress < 55)
							ThrowHook.Invoke(boxes[i].transform, boxes[i]);
		}

		/// <summary>
		/// Upgrade function
		/// </summary>
		public override void Upgrade()
		{
			int currentType = (int)type + 1;
			type = (MarkTypes)currentType;

			switch (type)
			{
				case MarkTypes.Mk1:
					finalTimeRange = mk1TimeRange;
					break;
				case MarkTypes.Mk2:
					finalTimeRange = mk2TimeRange;
					break;
				case MarkTypes.Mk3:
					finalTimeRange = mk3TimeRange;
					break;
				default:
					finalTimeRange = mk1TimeRange;
					break;
			}
		}

		/// <summary>
		/// SpawnBox gets called by the update and can be made to spawn in different power up boxes once we make them
		/// </summary>
		private void SpawnBox() => boxes.Add(Instantiate(boxPrefabs[UnityEngine.Random.Range(0, boxPrefabs.Count)]).Init(ThrowHook, transform, powerUpManager));
		/// <summary>
		/// OnThrowHook is the function used to make it possibe to grab the floating boxes and bring them back towards the crane
		/// </summary>
		/// <param name="boxTransform">the transform of the box</param>
		/// <param name="boxBehaviour">the box behavior of the box</param>
		private void OnThrowHook(Transform boxTransform, BoxBehaviour boxBehaviour)
		{
			LeanTween.cancel(hook);
			if (!shouldDoPlayerBehavior && !shoulDoCrewBehavior)
				return;
			boxes.Remove(boxBehaviour);
			boxBehaviour.StopCrate();
			Vector3 boxLocation = new Vector3(boxTransform.position.x, boxTransform.position.y, boxTransform.position.z + -4);
			if (!soundIsPlaying)
			{
				sound.PlayOneShotSound();
				soundIsPlaying = true;
			}
			//For some reason this get's skipped if the crate is to far through it's trip.
			LeanTween.move(hook, boxLocation, 1f)
				.setOnComplete(() =>
				{
					LeanTween.move(hook, CraneLocation, 1f);
					LeanTween.move(boxTransform.gameObject, CraneLocation, 1f).setOnComplete(() =>
						soundIsPlaying = false);
				});
		}
	}
}