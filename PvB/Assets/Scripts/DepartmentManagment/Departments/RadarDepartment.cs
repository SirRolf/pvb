using PvB.KM.CrewManagment;
using PvB.KM.CustomCamera;
using PvB.KM.Popup;
using PvB.KM.Sound.Shooting;
using PvB.KM.Upgrades;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.CrewManager.DepartmentManagment.Departments
{
    /// <summary>
    /// Department class for the Radar Department
    /// </summary>
    public class RadarDepartment : Department
    {
        /// <summary>
        /// The main cameraBehavior
        /// </summary>
        [SerializeField]
        private CameraBehavior cameraBehavior;
        /// <summary>
        /// The main camera
        /// </summary>
        [SerializeField]
        private Camera camera;

        /// <summary>
        /// Transform for the enemy boat popup
        /// </summary>
        [Header("Pop-ups")]
        [SerializeField]
        private List<PopupBehavior> popups;

        [Header("Adjustments")]
        /// <summary>
        /// How far the radar zooms out
        /// </summary>
        [SerializeField]
        private int mk1Zoom;
        /// <summary>
		/// Adds up to radarZoom on Mk2
		/// </summary>
        [SerializeField]
        private int mk2Zoom;
        /// <summary>
		/// Adds up to radarZoom on Mk3
		/// </summary>
        [SerializeField]
        private int mk3Zoom;
        /// <summary>
		/// Enum with Marks
		/// </summary>
        private MarkTypes type = MarkTypes.Mk1;
        /// <summary>
        /// how far back popups get placed closer to the center of the screen, in this case just the width
        /// </summary>
        [SerializeField]
        private float popupbarrierWidth;
        /// <summary>
        /// how far back popups get placed closer to the center of the screen, in this case just the height
        /// </summary>
        [SerializeField]
        private float popupbarrierHeight;
        /// <summary>
        /// If you should do the crew behavior in  the update 
        /// </summary>
        private bool shouldDoCrewBehavior;

		/// <summary>
		/// Sound effect for the Crane
		/// </summary>
		[SerializeField]
		private ShootingSound sound;
		/// <summary>
		/// Bool to prevent clipping of audio.
		/// </summary>
		private bool soundIsPlaying = false;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void Initialize()
        {
            type = MarkTypes.Mk1;
            return;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void CleanUp()
        {
            return;
        }

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Activate(DepartmentStatus status)
		{
            int totalZoom;

            if (status == DepartmentStatus.Player)
			{
                switch (type)
                {
                    case MarkTypes.Mk1:
                        totalZoom = mk1Zoom;
                        break;
                    case MarkTypes.Mk2:
                        totalZoom = mk2Zoom;
                        break;
                    case MarkTypes.Mk3:
                        totalZoom = mk3Zoom;
                        break;
                    default:
                        totalZoom = mk1Zoom;
                        break;
                }
                cameraBehavior.MoveCamera(Util.Axis.Y, totalZoom);
            }
                
            if (status == DepartmentStatus.CrewMember)
                shouldDoCrewBehavior = true;
			sound.PlaySound();
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void Disable()
        {
			sound.StopSound();
            if (shouldDoCrewBehavior)
            {
                for (int i = 0; i < popups.Count; i++)
                    popups[i].gameObject.SetActive(false);
                shouldDoCrewBehavior = false;
            }
            else
                cameraBehavior.ResetCameraPosition(Util.Axis.Y);
        }

		/// <summary>
		/// To make the markers move with the objects.
		/// </summary>
        private void Update()
        {
            if (shouldDoCrewBehavior)
            {

                for (int i = 0; i < popups.Count; i++)
                {
                    Vector3 screenPoint = GetScreenPoint(popups[i].GetLocation(), out bool disableBool);
                    popups[i].SetActive(disableBool);
                    popups[i].ChangePosition(screenPoint);
                    popups[i].ChangeRotation(Quaternion.Euler(0, 0, GetRotationToScreenCenter(screenPoint)));
                }
            }
        }

		/// <summary>
		/// Upgrade function
		/// </summary>
		public override void Upgrade()
		{
			int currentType = (int)type + 1;
			type = (MarkTypes)currentType;
		}

		/// <summary>
		/// returns the screenpoint of the given world space vector3
		/// </summary>
		/// <param name="value">World space vector3</param>
		/// <param name="wentOutOfBounds">if the player went out of bounds or not</param>
		/// <returns>the screenpoint</returns>
		private Vector3 GetScreenPoint(Vector3 value, out bool wentOutOfBounds)
        {
            wentOutOfBounds = false;
            Vector3 screenPoint = camera.WorldToScreenPoint(value);
            if (screenPoint.x > Screen.width - popupbarrierWidth)
            {
                screenPoint = new Vector3(Screen.width - popupbarrierWidth, screenPoint.y, 0);
                wentOutOfBounds = true;
            }
            else if (screenPoint.x < popupbarrierWidth)
            {
                screenPoint = new Vector3(popupbarrierWidth, screenPoint.y, 0);
                wentOutOfBounds = true;
            }
            if (screenPoint.y > Screen.height - popupbarrierHeight)
            {
                screenPoint = new Vector3(screenPoint.x, Screen.height - popupbarrierHeight, 0);
                wentOutOfBounds = true;
            }
            else if (screenPoint.y < popupbarrierHeight)
            {
                screenPoint = new Vector3(screenPoint.x, popupbarrierHeight, 0);
                wentOutOfBounds = true;
            }
            return screenPoint;
        }

        /// <summary>
        /// gets an angle to the center of the screen
        /// </summary>
        /// <param name="screenPoint">object of the location of the item you want to be pointed to the center of the screen</param>
        /// <returns>angle to the center of the screen</returns>
        private float GetRotationToScreenCenter(Vector3 screenPoint)
        {
            Vector2 CenterofScreen = new Vector2(Screen.width * .5f, Screen.height * .5f);
            CenterofScreen.x = CenterofScreen.x - screenPoint.x;
            CenterofScreen.y = CenterofScreen.y - screenPoint.y;
            return -(Mathf.Atan2(CenterofScreen.x, CenterofScreen.y) * Mathf.Rad2Deg);
        }
    }
}
