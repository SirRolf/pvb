using PvB.KM.BurningBoat;
using PvB.KM.CrewManagment;
using PvB.KM.CustomCamera;
using PvB.KM.GameOver;
using PvB.KM.Sound.Shooting;
using PvB.KM.Util;
using PvB.KM.Upgrades;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.CrewManager.DepartmentManagment.Departments
{
	/// <summary>
	/// Department class for the water cannon Department
	/// </summary>
	public class WaterCannonDepartment : Department
	{

		/// <summary>
		/// X offset for the camera
		/// </summary>
		private const int X_OFFSET = -20;
		/// <summary>
		/// Transform of this object
		/// </summary>
		[SerializeField]
		private Transform waterCannon;
		/// <summary>
		/// The main camera in the game
		/// </summary>
		[SerializeField]
		private Camera camera;
		/// <summary>
		/// The main camera behavior
		/// </summary>
		[SerializeField]
		private CameraBehavior cameraBehavior;
		/// <summary>
		/// The water beam
		/// </summary>
		[Header("WaterBeamComponents")]
		[SerializeField]
		private GameObject waterBeam;
		/// <summary>
		/// Hitbox of the waterspray
		/// </summary>
		[SerializeField]
		private BoxCollider waterLength;
		/// <summary>
		/// Collider for the water beam
		/// </summary>
		[SerializeField]
		private ColliderHelper waterBeamCollider;
		/// <summary>
		/// Collider Z axis Mk2
		/// </summary>
		[SerializeField]
		private float mk2SizeJump;
		/// <summary>
		/// Collider Z axis Mk3
		/// </summary>
		[SerializeField]
		private float mk3SizeJump;
		/// <summary>
		/// Particle system for the water spray
		/// </summary>
		[SerializeField]
		private ParticleSystem waterPart;
		/// <summary>
		/// Rotation speed of the player
		/// </summary>
		[Header("Player adjustment")]
		[SerializeField]
		[Range(0.1f, 1.5f)]
		private float rotationSpeed;
		/// <summary>
		/// Rotation speed for the crew
		/// </summary>
		[Header("Crew Adjustments")]
		[SerializeField]
		private float turnSpeed;
		/// <summary>
		/// Enum with marks.
		/// </summary>
		private MarkTypes type = MarkTypes.Mk1;
		/// <summary>
		/// If you should do the player behavior in  the update 
		/// </summary>
		private bool shouldDoPlayerBehavior;
		/// <summary>
		/// If you should do the crew behavior in  the update 
		/// </summary>
		private bool shouldDoCrewBehavior;
		/// <summary>
		/// If the crew is trying to hover over the target
		/// </summary>
		private bool aiIsLockingOn;
		/// <summary>
		/// Minimum time for the <see cref="boatSpawnTime"/>
		/// </summary>
		[Header("Boat Adjustments")]
		[SerializeField]
		private float boatSpawnMinTime;
		/// <summary>
		/// Maximum time for the <see cref="boatSpawnTime"/>
		/// </summary>
		[SerializeField]
		private float boatSpawnMaxTime;
		/// <summary>
		/// Timer for spawning boats
		/// </summary>
		private float boatSpawnTimer;
		/// <summary>
		/// Amount of time in between boat spawns
		/// </summary>
		private float boatSpawnTime;
		/// <summary>
		/// The list of diffrent boats that will try and attack you
		/// </summary>
		[SerializeField]
		private List<BoardingBoatBehavior> boatPrefabs;
		/// <summary>
		/// List of boats spawned
		/// </summary>
		private List<BoardingBoatBehavior> boats = new List<BoardingBoatBehavior>();
		/// <summary>
		/// Waterspray sound effect.
		/// </summary>
		[SerializeField]
		private ShootingSound sound;
		/// <summary>
		/// To check if sound is playing.
		/// </summary>
		private bool soundIsPlaying = false;
		/// <summary>
		/// If the update function should function
		/// </summary>
		private bool shouldUpdate;
        /// <summary>
        /// keeps track if the game has ended so it knows when to stop spawning
        /// </summary>
        private bool isGameOver = false;
        /// <summary>
        /// reference to a Victory GameOverManager 
        /// </summary>
        [SerializeField]
        private GameOverManager GameWin;
        /// <summary>
        /// reference to the GameOver GameOverManager 
        /// </summary>
        [SerializeField]
        private GameOverManager GameOver;
		/// <summary>
		/// Pausescreen.
		/// </summary>
		[SerializeField]
		private GameObject pauseScreen;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void Initialize()
        {
            shouldUpdate = true;
            GameOver.OnStop += OnGameEnd;
            GameWin.OnStop += OnGameEnd;
        }

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void CleanUp()
        {
            shouldUpdate = false;
            GameOver.OnStop -= OnGameEnd;
        }

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Activate(DepartmentStatus status)
		{
			shouldDoPlayerBehavior = status == DepartmentStatus.Player;
			shouldDoCrewBehavior = status == DepartmentStatus.CrewMember;
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.MoveCamera(Axis.X, waterCannon.position.x + X_OFFSET);
				cameraBehavior.MoveCamera(Axis.Z, waterCannon.position.z);
				cameraBehavior.RotateCamera(Axis.Y, 270);
			}
			if (shouldDoCrewBehavior)
			{		
				//water.SetActive(true);
				waterBeam.SetActive(true);
			}
			waterBeamCollider.Collided += BoatCollided;
		}

		/// <summary>
		/// <inheritdoc/>
		/// </summary>
		public override void Disable()
		{
			if (shouldDoPlayerBehavior)
			{
				cameraBehavior.ResetCameraPosition(Axis.X);
				cameraBehavior.ResetCameraPosition(Axis.Z);
				cameraBehavior.ResetCameraRotation(Axis.Y);
			}
			//Removes sound effect when it gets disabled all together.
			if (soundIsPlaying)
			{
				sound.StopSound();
				soundIsPlaying = false;
			}
			//water.SetActive(false);
			waterBeam.SetActive(false);
			shouldDoPlayerBehavior = false;
			shouldDoCrewBehavior = false;
			waterBeam.SetActive(false);
			waterBeamCollider.Collided -= BoatCollided;
		}

		/// <summary>
		/// Returns the closest boat
		/// </summary>
		/// <returns>The closest boat</returns>
		public BoardingBoatBehavior getClosestBoat() => boats[0];

		private void Update()
		{
			if (!shouldUpdate)
				return;
			boatSpawnTimer += Time.deltaTime;
			if (boatSpawnTimer > boatSpawnTime && !isGameOver)
				SpawnBoardingBoat();
			if (shouldDoCrewBehavior && !aiIsLockingOn)
				CrewBehavior();
			if (shouldDoPlayerBehavior)
				PlayerBehavior();
		}

		/// <summary>
		/// Mark upgrade function
		/// </summary>
		public override void Upgrade()
		{
			int currentType = (int)type + 1;
			type = (MarkTypes)currentType;

			switch (type)
			{
				case MarkTypes.Mk1:
					break;
				case MarkTypes.Mk2:
					waterLength.size = new Vector3(waterLength.size.x, waterLength.size.y, waterLength.size.z + mk2SizeJump);
					waterPart.startLifetime = 0.88f;
					break;
				case MarkTypes.Mk3:
					waterLength.size = new Vector3(waterLength.size.x, waterLength.size.y, waterLength.size.z + mk3SizeJump);
					waterPart.startLifetime = 1f;
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Behavior for the crew trying to shoot the cannon
		/// </summary>
		private void CrewBehavior()
		{
			//calculating the angle			
			if (boats[0] == null)
				return;
			Vector3 closestBoatLocation = boats[0].transform.position;// use GetClosestBoatPosition(); when implemented
			Vector3 relativePos = closestBoatLocation - waterCannon.position;
			Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
			Quaternion finalRotation = Quaternion.RotateTowards(transform.rotation, rotation, turnSpeed * Time.deltaTime);
			finalRotation.eulerAngles = new Vector3(0, finalRotation.eulerAngles.y, 0);
			transform.rotation = finalRotation;

			//Starts the sound effect when the crew gets on it.
			if (!soundIsPlaying && pauseScreen.activeInHierarchy == false)
			{
				sound.PlaySound();
				soundIsPlaying = true;
			}
			else if (pauseScreen.activeInHierarchy == true)
			{
				sound.StopSound();
				soundIsPlaying = false;
			}
		}

		/// <summary>
		/// Behavior for the player trying to shoot the cannon
		/// </summary>
		private void PlayerBehavior()
		{
			//Get the new angle the cannon should rotate towards
			Vector3 mousePos = Input.mousePosition;
			Vector3 objectPos = camera.WorldToScreenPoint(waterCannon.position);
			mousePos.x = mousePos.x - objectPos.x;
			mousePos.y = mousePos.y - objectPos.y;
			float angle = -(Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg);
			//restricting the rotation to 180 degrees
			if (angle > 0 && angle < 90)
				angle = 0;
			else if (angle > 90 && angle < 180)
				angle = 180;
			//the rotating
			LeanTween.cancel(waterCannon.gameObject);
			LeanTween.rotateY(waterCannon.gameObject, angle, rotationSpeed);

			if (Input.GetMouseButton(0))
			{
				//Starts sound effect on hold
				if (!soundIsPlaying && pauseScreen.activeInHierarchy == false)
				{
					sound.PlaySound();
					soundIsPlaying = true;
				}
				else if(pauseScreen.activeInHierarchy == true)
				{
					sound.StopSound();
					soundIsPlaying = false;
				}
				//water.SetActive(true);
				waterBeam.SetActive(true);
			}
			else
			{
				//Removes sound effect on release
				if (soundIsPlaying)
				{
					sound.StopSound();
					soundIsPlaying = false;
				}
				//water.SetActive(false);
				waterBeam.SetActive(false);
			}
		}

		/// <summary>
		/// Spawns a boarding boat trying to attack the waterCannon
		/// </summary>
		private void SpawnBoardingBoat()
		{
			boatSpawnTime = Random.Range(boatSpawnMinTime, boatSpawnMaxTime);
			boatSpawnTimer = 0;
			boats.Add(Instantiate(boatPrefabs[Random.Range(0, boatPrefabs.Count)]).Init(waterCannon));
			SortBoats();
		}

		/// <summary>
		/// Removes the boat thatcollided with the waterBeam
		/// </summary>
		/// <param name="collision">The collider from the boat collided with the player</param>
		public void BoatCollided(Collider collision)
		{
			for (int i = 0; i < boats.Count; i++)
				if (collision.gameObject == boats[i].gameObject && boats[i] != null)
				{
					boats[i].DestroyBoat();
					boats.RemoveAt(i);
					return;
				}
			SortBoats();
		}

		/// <summary>
		/// Sorts boats. sets the closest boat to the first spot in the list
		/// </summary>
		private void SortBoats()
		{
			int closestBoatIndex = 0;
            if (boats[closestBoatIndex] == null)
                boats.RemoveAt(closestBoatIndex);
            float closestBoatDistance = boats[closestBoatIndex].GetDistanceFromTarget();
			for (int i = 0; i < boats.Count; i++)
			{
				float distanceFromTarget = boats[i].GetDistanceFromTarget();
				if (distanceFromTarget < closestBoatDistance)
				{
					closestBoatDistance = distanceFromTarget;
					closestBoatIndex = i;
				}
			}
			BoardingBoatBehavior item = boats[closestBoatIndex];
			boats.RemoveAt(closestBoatIndex);
			boats.Insert(0, item);
		}
        /// <summary>
        /// gets called when you win the game makes it so no more boarding boats spawn
        /// and all existing boarding boats get a new target
        /// </summary>
        public void OnGameEnd()
        {
            isGameOver = true;
            for (int i = 0; i < boats.Count; i++)
                boats[i].SwitchToSecondaryTarget();
        }
	}
}
