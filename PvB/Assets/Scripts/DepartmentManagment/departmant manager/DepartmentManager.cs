using System.Collections.Generic;
using UnityEngine;
using PvB.KM.CrewManagment;
using PvB.KM.CrewManager.DepartmentManagment;

namespace PvB.KM.DepartmentManagment
{
    /// <summary>
    /// DepartmentManager handles the activation and disabling of the various departments
    /// </summary>
    public class DepartmentManager : MonoBehaviour
    {   /// <summary>
        /// departmenttypes hold the department types to later fill the dictionary with 
        /// </summary>
        [Header("the department types part of the dictionary")]
        [SerializeField] 
        private List<DepartmentType> departmenttypes;
        /// <summary>
        /// departmentclasses hold the department classes to later fill the dictionary with 
        /// </summary>
        [Header("the department classes part of the dictionary")]
        [SerializeField]
        private List<Department> departmentclasses;
        /// <summary>
        /// the dictionary made from departmenttypes and departmentclasses in CombineListstoDictionary
        /// and used in OnDepartmentrefresh.
        /// </summary>
        private Dictionary<DepartmentType, Department> departments = new Dictionary<DepartmentType, Department>();

        /// <summary>
        /// calls all the <see cref="Department.Initialize"/> of all the departments
        /// </summary>
        public void StartDepartments()
        {
            foreach (DepartmentType type in departmenttypes)
                departments[type].Initialize();
        }

        /// <summary>
        /// calls all the <see cref="Department.CleanUp"/> of all the departments
        /// </summary>
        public void StopDepartments()
        {
            foreach (DepartmentType type in departmenttypes)
                departments[type].CleanUp();
        }

        private void OnEnable()
        {
            CombineListstoDictionary();
            CrewMemberManager.Refresh += OnDepartmentrefresh;
        }
        private void OnDisable()=>CrewMemberManager.Refresh -= OnDepartmentrefresh;
        
        /// <summary>
        /// transforms departmenttypes and departmentclasses and fills departments with them
        /// </summary>
        public void CombineListstoDictionary()
        {
            for (int i = 0; i < departmenttypes.Count; i++)
                if (i < departmentclasses.Count)                
                    departments.Add(departmenttypes[i], departmentclasses[i]);               
        }
        /// <summary>
        /// takes the given department and activates/disables it depending on the given status 
        /// </summary>
        /// <param name="department">the department that was updated</param>
        /// <param name="status">the new status of the updated department</param>
        public void OnDepartmentrefresh(DepartmentType department, DepartmentStatus status)
        {
            if (status == DepartmentStatus.Empty)            
                departments[department].Disable();            
            else           
                departments[department].Activate(status);
        }
    }
}