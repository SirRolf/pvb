using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Brandon Ruigrok
namespace PvB.KM.MainMenu
{
	/// <summary>
	/// Handles all dip to blacks.
	/// </summary>
	public class BlackFades : MonoBehaviour
	{
		/// <summary>
		/// Canvas for the fade out.
		/// </summary>
		[SerializeField]
		private CanvasGroup fade;
		/// <summary>
		/// Black image in the fade CanvasGroup.
		/// </summary>
		[SerializeField]
		private Image black;

		/// <summary>
		/// Fades the screen to black.
		/// </summary>
		public void FadesOut(Action onComplete)
		{
			black.enabled = true;   //Enables the picture that overlays the main menu.
			LeanTween.alphaCanvas(fade, 1f, 1.5f)
				.setEase(LeanTweenType.easeInQuart)
				.setOnComplete(() => onComplete.Invoke());
		}

		/// <summary>
		/// Cleans up the dip to black
		/// </summary>
		public void FadeClean()
		{
			LeanTween.alphaCanvas(fade, 0f, 1.5f)
				.setEase(LeanTweenType.easeInQuart);
			//Once the prefabs of the game are made, you can call this function, 
			//if it instantiates in another script.
		}

		/// <summary>
		/// Partially fades out the screen.
		/// </summary>
		public void PartialFade()
		{
			LeanTween.alphaCanvas(fade, 0.4f, 0.5f)
				.setEase(LeanTweenType.easeInQuart);
		}

		/// <summary>
		/// Cleans the partial fade.
		/// </summary>
		public void PartialClean()
		{
			LeanTween.alphaCanvas(fade, 0f, 0.5f)
				.setEase(LeanTweenType.easeOutQuart);
		}
	}
}