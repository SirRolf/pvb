using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PvB.KM.CrewManagment;
using PvB.KM.Sound.UI;
using PvB.KM.Sound.Theme;

//Brandon Ruigrok
namespace PvB.KM.GameOver
{
	public class GameOverBehaviour : MonoBehaviour
	{
		/// <summary>
		/// Button to restart the scene.
		/// </summary>
		[SerializeField]
		private Button retry;
		/// <summary>
		/// Soundtrack of the game.
		/// </summary>
		[SerializeField]
		private GameTheme music;

		/// <summary>
		/// Function to add the restart function to the button.
		/// </summary>
		private void OnEnable()
		{
			CrewMemberManager.UnasignAllCrew();
			retry.onClick.AddListener(() =>
			{
				music.changeTheme(MusicTypes.soundDeletion);
				ButtonSound.playSound();
				SceneManager.LoadScene("MainBranch");
			});
		}
	}
}