using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.MainMenu;
using PvB.KM.DepartmentManagment;
using PvB.KM.Sound.Theme;
using PvB.KM.CrewManager.DepartmentManagment.Departments;
using System;

//Brandon Ruigrok
namespace PvB.KM.GameOver
{
    public class GameOverManager : MonoBehaviour
    {
        /// <summary>
        /// reference to player collider to disable during Stop();
        /// </summary>
        [SerializeField]
        private BoxCollider playerCol;
        /// <summary>
        /// Dip to black and clean up.
        /// </summary>
        [SerializeField]
        private CanvasGroup gameOverBackground;
        /// <summary>
        /// Background for the icon
        /// </summary>
        [SerializeField]
        private RectTransform iconBackground;
        /// <summary>
        /// The icon on the iconbackground
        /// </summary>
        [SerializeField]
        private RectTransform Icon;
        /// <summary>
        /// The background for the exit button
        /// </summary>
        [SerializeField]
        private RectTransform buttonBackground;
        /// <summary>
        /// Everything related to the game over screen.
        /// </summary>
        [SerializeField]
        private GameObject gameOver;
        /// <summary>
        /// the department manager of the game
        /// </summary>
        [SerializeField]
        private DepartmentManager departmentManager;
        /// <summary>
        /// Soundtrack of the game.
        /// </summary>
        [SerializeField]
        private GameTheme music;
        /// <summary>
        /// an action called when the game ends
        /// </summary>
        public Action OnStop;

        /// <summary>
        /// Cleans the game, and shows the game over screen.
        /// </summary>
        public void Stop()
        {
            OnStop();
            playerCol.enabled = false;
            departmentManager.StopDepartments();
            music.changeTheme(MusicTypes.vicTheme);
            gameOver.SetActive(true);
            iconBackground.position = new Vector3(iconBackground.position.x, Screen.height, iconBackground.position.z);
            buttonBackground.position = new Vector3(buttonBackground.position.x, -Screen.height, buttonBackground.position.z);
            LeanTween.alphaCanvas(gameOverBackground, 1f, 1f)
                .setEase(LeanTweenType.easeOutQuad);
            gameOverBackground.interactable = true;
            gameOverBackground.blocksRaycasts = true;
            LeanTween.move(buttonBackground, new Vector3(), 1f);
            LeanTween.move(iconBackground, new Vector3(), 1f)
                .setEase(LeanTweenType.easeOutQuad)
                .setOnComplete(() =>
                {
                    LeanTween.scale(Icon, new Vector3(1.2f, 1.2f, 1.2f), .5f)
                        .setEase(LeanTweenType.easeInBack)
                        .setOnComplete(() =>
                        {
                            LeanTween.rotateZ(Icon.gameObject, 10, .2f)
                                .setOnComplete(() =>
                                {
                                    LeanTween.rotateZ(Icon.gameObject, -10, .2f)
                                        .setOnComplete(() =>
                                        {
                                            LeanTween.rotateZ(Icon.gameObject, 0, .1f);
                                            LeanTween.scale(Icon, new Vector3(1f, 1f, 1f), 1f)
                                                .setEase(LeanTweenType.easeOutQuart);
                                        });
                                });
                        });
                });
        }
    }
}