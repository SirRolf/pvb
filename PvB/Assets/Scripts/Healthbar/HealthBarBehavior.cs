using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Healthbar
{
	/// <summary>
	/// behavior for a bar that shows the health somebody has left
	/// </summary>
	public class HealthBarBehavior : MonoBehaviour
	{
		/// <summary>
		/// The bar that shows the health
		/// </summary>
		[SerializeField]
		private RectTransform bar;
		/// <summary>
		/// moves the bar. No need to calculate just throw in the max health and current health
		/// </summary>
		/// <param name="maxHealth">the maximum health of user</param>
		/// <param name="currentHealth">the current health the user has</param>
		public void UpdateHealth(float maxHealth, float currentHealth)
		{
			LeanTween.cancel(gameObject);
			Vector2 maxAnchor = bar.anchorMax;
			LeanTween.value(gameObject, maxAnchor.x, currentHealth / maxHealth, .5f)
				.setEase(LeanTweenType.easeOutQuart)
				.setOnUpdate((float amount) => bar.anchorMax = new Vector2(amount, 1f));
		}
	}
}
