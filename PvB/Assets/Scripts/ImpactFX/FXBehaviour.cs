using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXBehaviour : MonoBehaviour
{
    /// <summary>
    /// Sets the lifetime of the effexts till it gets destroyed
    /// </summary>
    [SerializeField]
    private float lifetime;
    ///<summary>
    /// Destroys the impact effect after a certain time
    ///</summary>
    [SerializeField]
    private GameObject Effects;

    ///<summary>
    /// Calls the coroutine
    ///</summary>
    private void Start() => StartCoroutine(Wait());
    
    ///<summary>
    /// Sets the timer for the effects once the coroutine gets called
    ///</summary>
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(Effects);   
    }
}
