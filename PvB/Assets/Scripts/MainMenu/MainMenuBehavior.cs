using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PvB.KM.Start;
using PvB.KM.CustomCamera;

//Brandon Ruigrok
namespace PvB.KM.MainMenu
{
	/// <summary>
	/// Manages all the components in the main menu.
	/// </summary>
	public class MainMenuBehavior : MonoBehaviour
	{
		/// <summary>
		/// Start up script.
		/// </summary>
		[SerializeField]
		private StartManager startManager;
		/// <summary>
		/// Buttons for starting the game.
		/// </summary>
		[SerializeField]
		private Button Next;
		/// <summary>
		/// Button for closing the game.
		/// </summary>
		[SerializeField]
		private Button Close;
		/// <summary>
		/// The Main camerabehavior
		/// </summary>
		[SerializeField]
		private CameraBehavior camera;
		/// <summary>
		/// The location for where the camera should go
		/// </summary>
		[SerializeField]
		private Transform menuCameraLocation;
		/// <summary>
		/// Canvas group to fade out the buttons.
		/// </summary>
		[SerializeField]
		private CanvasGroup menu;
		/// <summary>
		/// The fade like background in the main menu
		/// </summary>
		[SerializeField]
		private CanvasGroup fadeBackground;
		/// <summary>
		/// Menu transform
		/// </summary>
		[SerializeField]
		private RectTransform menuTransform;
		/// <summary>
		/// ScoreBoardTransform transform
		/// </summary>
		[SerializeField]
		private RectTransform scoreBoardTransform;

		/// <summary>
		/// Adds the OnClick function to the buttons.
		/// </summary>
		private void OnEnable()
		{
			camera.SetCameraPositions();
			camera.TransformLocationCamera(menuCameraLocation.position);
			camera.TransformRotationCamera(menuCameraLocation.rotation);
			// Starts the start up sequence.
			Next.onClick.AddListener(startManager.StartUp);
			// Closes the game upon clicking.
			Close.onClick.AddListener(Application.Quit);
		}

		/// <summary>
		/// Removes all listeners from the Main Menu buttons.
		/// </summary>
		private void OnDisable()
		{
			Next.onClick.RemoveAllListeners();
			Close.onClick.RemoveAllListeners();
		}

		/// <summary>
		/// Hides the canvas of the main menu.
		/// </summary>
		public void HideMain()
		{
			menu.interactable = false;
			menu.blocksRaycasts = false;
			LeanTween.move(menuTransform, -menuTransform.position, 1f)
				.setEase(LeanTweenType.easeInBack);
			LeanTween.alphaCanvas(fadeBackground, 0f, 1f)
				.setEase(LeanTweenType.easeInCubic);
			LeanTween.moveX(scoreBoardTransform, scoreBoardTransform.rect.width, 1f)
				.setEase(LeanTweenType.easeInBack);
		}
	}
}
