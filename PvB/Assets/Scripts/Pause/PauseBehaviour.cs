using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PvB.KM.Sound.Theme;

//Brandon Ruigrok
namespace PvB.KM.Pause
{
	/// <summary>
	/// Class for assigning buttons their functions.
	/// </summary>
	public class PauseBehaviour : MonoBehaviour
	{
		/// <summary>
		/// To be able bind Resume() function to continue button.
		/// </summary>
		[SerializeField]
		private PauseManager pauseManager;
		/// <summary>
		/// Soundtrack of the game.
		/// </summary>
		[SerializeField]
		private GameTheme music;

		/// <summary>
		/// Button for continueing.
		/// </summary>
		[SerializeField]
		private Button cont;
		/// <summary>
		/// Button for quiting.
		/// </summary>
		[SerializeField]
		private Button quit;

		/// <summary>
		/// Adds functions when it gets enabled.
		/// </summary>
		private void OnEnable()
		{
			cont.onClick.AddListener(() => pauseManager.Resume());
			quit.onClick.AddListener(() =>
			{
				//Has to resume first, otherwise it gets stuck on main menu.
				music.changeTheme(MusicTypes.soundDeletion);
				pauseManager.Resume();
				SceneManager.LoadScene("MainBranch");
			});
		}

		/// <summary>
		/// Removes functions from buttons when it gets disabled.
		/// </summary>
		private void OnDisable()
		{
			cont.onClick.RemoveAllListeners();
			quit.onClick.RemoveAllListeners();
		}
	}
}
