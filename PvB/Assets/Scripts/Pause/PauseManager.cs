using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.MainMenu;
using PvB.KM.Sound.UI;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

//Brandon Ruigrok
namespace PvB.KM.Pause
{
	public class PauseManager : MonoBehaviour
	{
		/// bool that tells if the pause manu can be activated
		/// </summary>
		public bool CanBeActivated { private get; set; }

		/// <summary>
		/// The pause screen object.
		/// </summary>
		[SerializeField]
		private GameObject pauseScreen;

		/// <summary>
		/// The game itself.
		/// </summary>
		[SerializeField]
		private GameObject game;

		/// <summary>
		/// A button used for pausing the game
		/// </summary>
		[SerializeField]
		private Button pauseButton;

		/// <summary>
		/// A boolean to see if the game is paused or not.
		/// </summary>
		private bool isPaused = false;

		/// <summary>
		/// Action for that gets called when you press esc or the pause button
		/// </summary>
		private UnityAction Pause;


		/*/// <summary>
		/// Script for fade in and outs.
		/// </summary>
		FOR FUTURE USE MAYBE.
		[SerializeField]
		private BlackFades fades;*/

		private void OnEnable()
		{
			Pause += OnPause;
			pauseButton.onClick.AddListener(Pause);
		}

		private void OnDisable()
		{
			pauseButton.onClick.RemoveListener(Pause);
			Pause -= OnPause;
		}

		/// <summary>
		/// Update function listens for the escape input
		/// </summary>
		void Update()
		{
			if (!CanBeActivated)
				return;
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				//then checks to see if the game is active and if the game is paused or not.
				if (game.activeInHierarchy && isPaused == false)
					OnPause();
				else if (game.activeInHierarchy && isPaused == true)
					Resume();
			}
		}

		/// <summary>
		/// Pausing function.
		/// </summary>
		public void OnPause()
		{
			//This is work in progress for a slight fade out on pause menu
			//fades.PartialFade();
			pauseScreen.SetActive(true);
			Time.timeScale = 0;
			isPaused = true;
		}

		/// <summary>
		/// Resuming function.
		/// </summary>
		public void Resume()
		{
			//This is work in progress for a slight fade out on resumation. 
			//fades.PartialClean();
			ButtonSound.playSound();
			pauseScreen.SetActive(false);
			Time.timeScale = 1;
			isPaused = false;
		}
	}
}