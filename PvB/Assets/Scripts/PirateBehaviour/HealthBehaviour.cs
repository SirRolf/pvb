using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles health functions
/// </summary>
public class HealthBehaviour 
{
	/// <summary>
	/// the amount of health
	/// </summary>
	private float health;
	/// <summary>
	/// the amount of health
	/// </summary>
	public float Health => health;
	/// <summary>
	/// The maximum health
	/// </summary>
	private float maxHealth;
	/// <summary>
	/// adds health to Health
	/// </summary>
	/// <param name="health">amount of initial health</param>
	public HealthBehaviour(int health)
	{
		this.health = health;
		maxHealth = health;
	}
	/// <summary>
	/// adds value to Health and returns true if health is les then zero
	/// </summary>
	/// <param name="value">amount of health to add</param>
	/// <returns></returns>
	public bool UpdateHealth(float value)
	{
		health += value;
		if (health <= 0)
			return true;
		else if (health > maxHealth)
		{
			health = maxHealth;
			return true;
		}
		else
			return false;
	}
}