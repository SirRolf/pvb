using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.Projectile;
namespace PvB.KM.Pirates
{
    /// <summary>
    /// behaviour script for the largeboat
    /// </summary>
    public class LargeBoat : PirateBehaviour
    {
        /// <summary>
        /// the axis on which this boat rotates
        /// </summary>
        public Vector3 axis;
        /// <summary>
        /// Reference to the player transformso the boat knows where to point to.
        /// </summary>
        public Transform playerTransform;
        /// <summary>
        /// Reference to the position from which projectiles are fired
        /// </summary>
        [SerializeField]
        private Transform shotPos;
        /// <summary>
        /// Reference to the projectiles which is fired
        /// </summary>
        [SerializeField]
        private ProjectileBehaviour projectile;
        /// <summary>
        /// Z cords the boat has to reach before it fires and starts cicling
        /// </summary>
        [SerializeField]
        private float distanceBeforeCircling;
        /// <summary>
        /// handles delays between shots
        /// </summary>
        private float shotDelay;
        /// <summary>
        /// the speed at which the boat moves
        /// </summary>
        private float speed = 3;
        /// <summary>
        /// list of all shot bullets
        /// </summary>
        [SerializeField]
        private List<GameObject> shotBullets;
        /// <summary>
        /// fils playerTransform & health
        /// </summary>
        private void OnEnable()
        {
            health = new HealthBehaviour(15);
            canShoot = false;
            transform.eulerAngles=new Vector3(0,90,0);
        }
        /// <summary>
        /// handles losing health on collion with projectile collisions
        /// </summary>
        /// <param name="collision">collided object</param>
        public override void OnTriggerEnter(Collider collision)
        {
            ProjectileBehaviour projectileBehaviour = collision.gameObject.GetComponent<ProjectileBehaviour>();
            if (!shotBullets.Contains(projectileBehaviour.gameObject))
            {
                projectileBehaviour.DisableProjectile();
                if (health.UpdateHealth(-projectileBehaviour.damage))
                    gameObject.SetActive(false);
            }
        }
        /// <summary>
        /// Instantiates the projectile
        /// </summary>
        public override void ShootPlayer()
        {
            ProjectileBehaviour bullet = Instantiate(projectile, shotPos.position, shotPos.rotation);
            shotDelay = 0;
            shotBullets.Add(bullet.gameObject);
        }
        /// <summary>
        /// count up the shot delay and handles the movement functions
        /// </summary>
        public override void Update()
        {
            shotDelay += Time.deltaTime;
            if (shotDelay > 10 && canShoot)
                ShootPlayer();
            if (!canShoot)
            {
                transform.Translate(Vector3.back * speed * Time.deltaTime,Space.World);
                if (transform.position.z <= distanceBeforeCircling)
                {
                    transform.LeanRotateY(0, 5);
                    canShoot = true;
                }
            }
            if (canShoot)
            {
                transform.RotateAround(playerTransform.position, axis, speed * Time.deltaTime);
                if (transform.position.z < 10)
                {
                    speed = speed * -1;
                    transform.LeanRotateY(-90, 0);                  
                }
            }
        }
        /// <summary>
        /// sets player transform
        /// </summary>
        /// <param name="newPlayerTransform">the new player transform</param>
        public void SetPlayerTransform(Transform newPlayerTransform) => playerTransform = newPlayerTransform;
        /// <summary>
        /// returns player transform
        /// </summary>
        /// <returns>returns player transform</returns>
        public Transform GetPlayerTransform() => playerTransform;
        /// <summary>
        /// when disabled call the disabled function
        /// </summary>
        private void OnDisable()
        {
            if (gameObject != null)
                disabled();
        }
    }
}
