using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Pirates
{
    /// <summary>
    /// a script that turns the gun towards the player
    /// </summary>
    public class LargeBoatTurret : MonoBehaviour
    {
        /// <summary>
        /// turns the gun towards the player
        /// </summary>
        void Update() => transform.LookAt(transform.GetComponentInParent<LargeBoat>().GetPlayerTransform(), Vector3.up);
    }
}

