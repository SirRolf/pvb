using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.Projectile;
using System;

namespace PvB.KM.Pirates
{
    /// <summary>
    /// base script for all pirate ships
    /// </summary>
    public abstract class PirateBehaviour : MonoBehaviour
    {
        public static Action disabled;
        /// <summary>
        /// health of ship
        /// </summary>
        protected HealthBehaviour health;
        /// <summary>
        /// tracks if the player can shoot
        /// </summary>
        protected bool canShoot;
        /// <summary>
        /// for all repeating logic
        /// </summary>
        public abstract void Update();
        /// <summary>
        /// for shooting logic
        /// </summary>
        public abstract void ShootPlayer();
        /// <summary>
        /// for collision logic
        /// </summary>
        /// <param name="collision"></param>
        public abstract void OnTriggerEnter(Collider collision);
    }

}
