using PvB.KM.Projectile;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Pirates
{
    /// <summary>
    /// enum of speed boat behaviour types
    /// </summary>
    public enum SpeedBoatType
    {
        kamikaze, stickclose, turnaround
    }
    /// <summary>
    /// handles the behaviour of the speedboats 
    /// </summary>
    public class SpeedBoat : PirateBehaviour
    {
        /// <summary>
        /// Reference to the player transformso the boat knows where to point to.
        /// </summary>
        Transform playerTransform;
        /// <summary>
        /// Reference to the position from which projectiles are fired
        /// </summary>
        [SerializeField]
        private Transform shotpos;
        /// <summary>
        /// Reference to the projectiles which is fired
        /// </summary>
        [SerializeField]
        private ProjectileBehaviour projectile;
        /// <summary>
        /// what type of speedboat it is determens its behaviour
        /// </summary>
        private SpeedBoatType type = SpeedBoatType.stickclose;
        /// <summary>
        /// handles delays between shots
        /// </summary>
        private float shotDelay;
        /// <summary>
        /// the speed at which the boat moves
        /// </summary>
        private const float SPEED = 3;
        /// <summary>
        /// distance between player boat and the stickclose boat
        /// </summary>
        [SerializeField]
        private int stickCloseDistance;
        /// <summary>
        /// list of all shot bullets
        /// </summary>
        [SerializeField]
        private List<GameObject> shotBullets;
        /// <summary>
        /// fils health & canshoot
        /// </summary>
        private void OnEnable()
        {
            health = new HealthBehaviour(10);
            canShoot = false;
        }
        /// <summary>
        /// handles losing health on collion with projectile collisions
        /// </summary>
        /// <param name="collision">collided object</param>
        public override void OnTriggerEnter(Collider collision)
        {
            ProjectileBehaviour projectileBehaviour = collision.gameObject.GetComponent<ProjectileBehaviour>();
            if (!shotBullets.Contains(projectileBehaviour.gameObject))
            {
                projectileBehaviour.DisableProjectile();
                if (health.UpdateHealth(-projectileBehaviour.damage))
                    gameObject.SetActive(false);
            }

            if (type == SpeedBoatType.kamikaze && collision.transform == playerTransform)
            {
                //updatepayerhealth - 50
                gameObject.SetActive(false);
            }
        }
        /// <summary>
        /// Instantiates the projectile
        /// </summary>
        public override void ShootPlayer()
        {
            ProjectileBehaviour bullet = Instantiate(projectile, shotpos.position, shotpos.rotation);
            shotDelay = 0;
            shotBullets.Add(bullet.gameObject);
        }
        /// <summary>
        /// count up the shot delay and calls the movement functions
        /// </summary>
        public override void Update()
        {
            shotDelay += Time.deltaTime;
            if (shotDelay > 5 && canShoot)
                ShootPlayer();
            transform.LookAt(playerTransform, Vector3.up);

            switch (type)
            {
                case SpeedBoatType.kamikaze:
                    KamikazeBehaviour();
                    break;
                case SpeedBoatType.stickclose:
                    StickCloseBehaviour();
                    break;
                case SpeedBoatType.turnaround:
                    turnaroundBehaviour();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// moves dirictly to the"Playertransform" 
        /// </summary>
        private void KamikazeBehaviour() => transform.Translate(Vector3.forward * SPEED * Time.deltaTime);
        /// <summary>
        /// moves dirictly to the player transform and then stops at a distance
        /// </summary>
        private void StickCloseBehaviour()
        {
            if (Vector3.Distance(transform.position, playerTransform.position) > stickCloseDistance)
                transform.Translate(Vector3.forward * SPEED * Time.deltaTime);
            else
                canShoot = true;
        }
        /// <summary>
        /// is the boat moving forward
        /// </summary>
        bool movingForward = true;
        /// <summary>
        /// moves dirictly to the playertransform and then turns around and then goes forward again etc.
        /// </summary>
        private void turnaroundBehaviour()
        {
            if (Vector3.Distance(transform.position, playerTransform.position) < stickCloseDistance)
            {
                canShoot = true;
                movingForward = false;
            } 
            else if (Vector3.Distance(transform.position, playerTransform.position) > stickCloseDistance + 20)
                movingForward = true;
            if (movingForward)
                transform.Translate(Vector3.forward * SPEED * Time.deltaTime);
            else
                transform.Translate(-Vector3.forward * SPEED * Time.deltaTime);
        }
        /// <summary>
        /// sets player transform
        /// </summary>
        /// <param name="newPlayerTransform">the new player transform</param>
        public void SetPlayerTransform(Transform newPlayerTransform) => playerTransform = newPlayerTransform;
        /// <summary>
        /// sets boattype
        /// </summary>
        /// <param name="type"></param>
        public void SetBoatType(SpeedBoatType type) => this.type = type;
        /// <summary>
        /// when disabled call the disabled function
        /// </summary>
        private void OnDisable()
        {
            if (gameObject!=null)
                disabled();
        }
    }
}