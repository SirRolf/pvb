using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
/// <summary>
/// this class loads a xml file that contains a wave array and deserializes that array into a usable array
/// </summary>
public class LoadWaves
{
    /// <summary>
    /// name of the xml file
    /// </summary>
    static string fileName = "Waves.Xml";
    /// <summary>
    /// loads the xml file that holds the waves deserializes it and returns it contents
    /// </summary>
    /// <returns>a wave array</returns>
   public static Wave[] LoadWaveList()
   {     
        XmlSerializer serializer = new XmlSerializer(typeof(Wave[]));
        string filePath = Application.streamingAssetsPath +"/"+fileName ;
        using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
        {
          Wave[] LoadedWaves = serializer.Deserialize(fileStream) as Wave[];
            return LoadedWaves;
        }
   }
}
