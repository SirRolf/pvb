using PvB.KM.Pirates;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PirateManager : MonoBehaviour
{
    /// <summary>
    /// list of all the boats
    /// </summary>
    [SerializeField]
    private List<PirateBehaviour> boats;
    /// <summary>
    /// reference to the player
    /// </summary>
    [SerializeField]
    private GameObject player;
    /// <summary>
    /// reference to speedboat 
    /// </summary>
    [SerializeField]
    private SpeedBoat speedBoat;
    /// <summary>
    /// reference to largeboat 
    /// </summary>
    [SerializeField]
    private LargeBoat largeBoat;
    /// <summary>
    /// maximum and minimum spawn co�rdinits
    /// </summary>
    [SerializeField]
    private float maxSpawnX, minSpawnX, maxSpawnY, minSpawnY;

    /// <summary>
    /// lets all subscribed function know all boats have been defeated 
    /// </summary>
    public Action allDefeated;
    /// <summary>
    /// adds all boats defeated to the disabled action
    /// </summary>
    private void OnEnable() => PirateBehaviour.disabled += AllBoatsDefeated;
    /// <summary>
    /// spawn a speedboat if there is a deactivated large in the boats it will list reset that boat else it will instantiate it 
    /// </summary>
    /// <param name="boatType">type of speedboat</param>
    public void CreateSpeedBoat(SpeedBoatType boatType)
    {
        Vector3 position = new Vector3(Random.Range(minSpawnX, maxSpawnX), 0, Random.Range(minSpawnY,maxSpawnY));
        foreach (PirateBehaviour item in boats)
        {
            if (!item.isActiveAndEnabled && item.GetComponent<SpeedBoat>() != null)
            {
                item.gameObject.SetActive(true);
                item.GetComponent<SpeedBoat>().SetBoatType(boatType);
                item.transform.position = position;
                return;
            }
        }
        SpeedBoat boat = Instantiate(speedBoat,position,Quaternion.identity);
        boat.SetPlayerTransform(player.transform);
        boat.SetBoatType(boatType);
        boats.Add(boat);
    }
    /// <summary>
    /// instantiate a large boat
    /// </summary>
    public void CreateLargeShip()
    {
        Vector3 position = new Vector3(0, 0, Random.Range(minSpawnY, maxSpawnY));

        foreach (PirateBehaviour item in boats)
        {
            if (!item.isActiveAndEnabled && item.GetComponent<LargeBoat>() != null)
            {
                item.gameObject.SetActive(true);
                item.transform.position = position;
                return;
            }
        }
        LargeBoat boat = Instantiate(largeBoat,position,Quaternion.identity);
        boat.SetPlayerTransform(player.transform);
        boats.Add(boat);
    }
    /// <summary>
    /// sorts boats and returs the vector of the closest boat
    /// </summary>
    /// <returns>vector of the closest pirates</returns>
    public Vector3 GetClosestBoatPosition()
    {
        boats.Sort(Sortposition);
        if (boats.Count != 0)
        {
            Vector3 closestPos = boats[0].transform.position;
            return closestPos;
        }
        else
            return Vector3.forward;
    }
    /// <summary>
    /// sort function sorts lists based on distance to the player from shortest to longest 
    /// </summary>
    /// <param name="a">the a value in the list</param>
    /// <param name="b">the b value in the list</param>
    /// <returns>a value wich id used List.Sort to determine the new order of the list</returns>
    private int Sortposition(PirateBehaviour a, PirateBehaviour b)
    {
        if (!a.isActiveAndEnabled)
            a.transform.position = new Vector3(0,0,2000);
        if (!b.isActiveAndEnabled)
             b.transform.position = new Vector3(0,0,2000);
       if (Vector3.Distance(player.transform.position,a.transform.position)  > Vector3.Distance(player.transform.position, b.transform.position))
            return 1;
        else if (Vector3.Distance(player.transform.position, a.transform.position) < Vector3.Distance(player.transform.position, b.transform.position))
            return -1;
        return -0;
    }
    /// <summary>
    /// returns true if all gameobjects in Boats als zijn gedeactiveert
    /// </summary>
    /// <returns>are all boats defeated</returns>
    public void AllBoatsDefeated()
    {
        bool allBoatsDefeated = true;
        for (int i = 0; i < boats.Count; i++)
        {
            if (boats[i].isActiveAndEnabled)
                allBoatsDefeated = false;
        }
        if (allBoatsDefeated)
            allDefeated();
        
    }
}
