/// <summary>
/// wave class is a class that the composition of a wave  
/// </summary>
[System.Serializable]
public class Wave
{
    /// <summary>
    /// number of kamikaze boats
    /// </summary>
   // public int kamikazeBoats { get; private set; }
    /// <summary>
    /// number of back and forth boats
    /// </summary>
    public int backAndForthgBoats;
    /// <summary>
    /// number of stay close boats
    /// </summary>
    public int StaycloseBoats;
    /// <summary>
    /// number of large boats
    /// </summary>
    public int largeboats;
}
