using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.Pirates;
using PvB.KM.GameOver;

public class WaveManegemant : MonoBehaviour
{
    /// <summary>
    /// holds GameOverManager for the VictoryScreen
    /// </summary>
    [SerializeField]
    private GameOverManager victory;
    /// <summary>
    /// current wave count
    /// </summary>
    private int WaveCount = 0;
    /// <summary>
    /// pirate manager reference
    /// </summary>
    [SerializeField]
    private PirateManager PirateManager;
    /// <summary>
    /// list of waves
    /// </summary>
    Wave[] waves;
    ///<summary>
    /// sets the action used in the progressbar
    ///</summary>
    public Action<float, float> WaveCompleted;
    /// <summary>
    /// sets up the waves and spaws the first
    /// </summary>
    public void StartWaves()
    {
        PirateManager.allDefeated += CallNewWave;
        waves = LoadWaves.LoadWaveList();
        SpawnNewWave(WaveCount);
    }
    /// <summary>
    /// spawn a new wave
    /// </summary>
    /// <param name="wavenumber">position of</param>
    public void SpawnNewWave(int wavenumber)
    {
        WaveCompleted(waves.Length - 1, WaveCount);

        if (wavenumber >= waves.Length)
        {
            victory.Stop();
            return;
        }
            
        //   for (int i = 0; i < waves[wavenumber].kamikazeBoats; i++)
        //    PirateManager.CreateSpeedBoat(SpeedBoatType.kamikaze);

        for (int i = 0; i < waves[wavenumber].backAndForthgBoats; i++)
            PirateManager.CreateSpeedBoat(SpeedBoatType.turnaround);

        for (int i = 0; i < waves[wavenumber].StaycloseBoats; i++)
            PirateManager.CreateSpeedBoat(SpeedBoatType.stickclose);

        for (int i = 0; i < waves[wavenumber].largeboats; i++)
            PirateManager.CreateLargeShip();
    }
    /// <summary>
    /// start corutine StartNewWave
    /// </summary>
    private void CallNewWave() => StartCoroutine(StartNewWave());
    /// <summary>
    /// waits for bit and then calls spawn new wave
    /// </summary>
    /// <returns></returns>
    IEnumerator StartNewWave()
    {
        yield return new WaitForSeconds(3);
        WaveCount++;
        SpawnNewWave(WaveCount);
    }
}
