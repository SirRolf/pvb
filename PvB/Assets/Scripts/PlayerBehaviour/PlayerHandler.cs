using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.GameOver;
using PvB.KM.Healthbar;
using PvB.KM.CrewManager.DepartmentManagment.Departments;

//Brandon Ruigrok
namespace PvB.KM.PlayerHandling
{
	public class PlayerHandler : MonoBehaviour
	{
		/// <summary>
		/// DISCLAIMER, HERE IS THE HEALTH VALUE FOR THE PLAYER.
		/// </summary>
		private const int HIT_POINTS = 30;

		/// <summary>
		/// To show the gameover screen
		/// </summary>
		[SerializeField]
		private GameOverManager gameOverScreen;

		/// <summary>
		/// Initiates health value for the player
		/// </summary>
		private HealthBehaviour healthHandler = new HealthBehaviour(HIT_POINTS);

		/// <summary>
		/// Hitbox for the player
		/// </summary>
		[SerializeField]
		private BoxCollider hitBox;

		/// <summary>
		/// healthbar for the player
		/// </summary>
		[SerializeField]
		private HealthBarBehavior healthBar;

		/// <summary>
		/// The watercannon for destroying boarding boats
		/// </summary>
		[SerializeField]
		private WaterCannonDepartment waterCannon;

		/// <summary>
		/// When a collider enters the trigger under the right condition, remove 1 hit point.
		/// </summary>
		/// <param name="other">Object with collision.</param>
		private void OnTriggerEnter(Collider other)
		{
			if (other.tag == "eBullet")
			{
				//Death condition.
				if (UpdateHealth(-1))
				{
					//Disables hitbox for the player.
					hitBox.enabled = false;
					//Calls the game over screen to appear.
					gameOverScreen.Stop();
				}
				//Destroys the bullet.
				Destroy(other.gameObject);
				waterCannon.BoatCollided(other);
			}
		}

		/// <summary>
		/// Updates health of the player
		/// </summary>
		/// <param name="amount">what to update it with</param>
		public bool UpdateHealth(float amount)
		{
			bool isDead = healthHandler.UpdateHealth(amount);
			healthBar.UpdateHealth(HIT_POINTS, healthHandler.Health);
			return isDead;
		}
	}
}