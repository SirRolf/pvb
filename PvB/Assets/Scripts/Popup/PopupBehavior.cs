using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Popup
{
    /// <summary>
    /// One of the popups for the radar
    /// </summary>
    public abstract class PopupBehavior : MonoBehaviour
    {
        /// <summary>
        /// Personal gameobject
        /// </summary>
        [SerializeField]
        private GameObject myGameObject;
        /// <summary>
        /// personal recttransform
        /// </summary>
        [SerializeField]
        public RectTransform transform;
        /// <summary>
        /// recttransform attached to the background of the popup
        /// </summary>
        [SerializeField]
        public RectTransform backgroundTransform;

        /// <summary>
        /// Gets the location of the point of interest
        /// </summary>
        /// <returns>Location of POI</returns>
        public abstract Vector3 GetLocation();

        /// <summary>
        /// Sets this game object to active or false
        /// </summary>
        /// <param name="value">SetActive to this value</param>
        public void SetActive(bool value) => myGameObject.SetActive(value);
        /// <summary>
        /// Changes the position of the popup
        /// </summary>
        /// <param name="value">where to move the position to</param>
        public void ChangePosition(Vector3 value) => transform.position = value;
        /// <summary>
        /// Rotate the popups background
        /// </summary>
        /// <param name="value">what to rotate the background to</param>
        public void ChangeRotation(Quaternion value) => backgroundTransform.rotation = value;
    }
}
