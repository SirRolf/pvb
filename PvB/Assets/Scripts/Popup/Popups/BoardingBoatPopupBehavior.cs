using PvB.KM.CrewManager.DepartmentManagment.Departments;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Popup
{
    /// <summary>
    /// popupbehavior for the burning boats. Points to the closest one
    /// </summary>
    public class BoardingBoatPopupBehavior : PopupBehavior
    {
        /// <summary>
        /// The department
        /// </summary>
        [SerializeField]
        private WaterCannonDepartment department;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns><inheritdoc/></returns>
        public override Vector3 GetLocation() => department.getClosestBoat().transform.position;//for now sending an  empty vector 3. Change this when the waterCannon is added
    }
}
