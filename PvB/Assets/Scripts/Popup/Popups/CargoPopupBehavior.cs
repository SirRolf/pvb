using PvB.KM.CrewManager.DepartmentManagment.Departments;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Popup
{
    /// <summary>
    /// popupbehavior for the cargo crates. points to the closest one
    /// </summary>
    public class CargoPopupBehavior : PopupBehavior
    {
        [SerializeField]
        CraneDepartment craneDepartment;
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns><inheritdoc/></returns>
        public override Vector3 GetLocation() => craneDepartment.Boxes[0].transform.position;
    }
}
