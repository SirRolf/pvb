using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.Pirates;

namespace PvB.KM.Popup
{
    /// <summary>
    /// popupbehavior for the pirate ships. Points to the closest ones
    /// </summary>
    public class EnemyBoatPopupBehavior : PopupBehavior
    {
        [SerializeField]
        private PirateManager pirateManager;
        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        /// <returns><inheritdoc/></returns>
        public override Vector3 GetLocation() => pirateManager.GetClosestBoatPosition();
    }
}
