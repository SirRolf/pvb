namespace PvB.KM.Powerups
{
	/// <summary>
	/// Types of chests you can find with the crane
	/// </summary>
	public enum ChestTypes
	{
		BigBox,
		SmallBox,
		Chest
	}
}
