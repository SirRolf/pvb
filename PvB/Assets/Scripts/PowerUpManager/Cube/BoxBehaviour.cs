using PvB.KM.Powerups;
using PvB.KM.ScoreSystems;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.PowerBox
{
	public class BoxBehaviour : MonoBehaviour
	{
		/// <summary>
		/// Is used to set the name on how to call the transform action
		/// </summary>
		public Action<Transform,BoxBehaviour> Click;
		/// <summary>
		/// Is used to set the lifetime for the cubes before they despawn
		/// </summary>
		[SerializeField]
		private float lifetime;
		/// <summary>
		/// The type of this chect
		/// </summary>
		[SerializeField]
		private ChestTypes type;
		/// <summary>
		/// Sets the speed at which the cube moves
		/// </summary>
		[SerializeField]
		private float speed;
		/// <summary>
		/// Sets the distance for the cubes to spawn away from the crane
		/// </summary>
		[SerializeField]
		private float DISTANCE_FROM_CRANE;
		/// <summary>
		/// Sets the positional min range for the cubes
		/// </summary>
		[SerializeField]
		private float adjustmentLeftFromPlayer;
		/// <summary>
		/// Sets the positional max range for the cubes
		/// </summary>
		[SerializeField]
		private float adjustmentRightFromPlayer;
		/// <summary>
		/// Is used to grab the transform of the crane
		/// </summary>
		private Transform Crane;
		/// <summary>
		/// Manager for starting the correct power ups
		/// </summary>
		private PowerUpManager powerUpManager;
		/// <summary>
		/// if the box should be moving
		/// </summary>
		private bool shouldMove = true;
		/// <summary>
		/// the timer for the lifetime of this box
		/// </summary>
		private float lifetimeTimer;
		/// <summary>
		/// How far the box is on it's way to his end of the yourney
		/// </summary>
		private float progress;
		/// <summary>
		/// How far the box is on it's way to his end of the yourney
		/// </summary>
		public float Progress => progress;

		/// <summary>
		/// The Update is here to make the cube move across the screen
		/// </summary>
		private void Update()
		{
			lifetimeTimer += Time.deltaTime;
			progress = lifetimeTimer / lifetime * 100;
			if (shouldMove)
			{
				Vector3 position = transform.position + transform.forward * speed * Time.deltaTime;
				position = new Vector3(position.x, -0.2f, position.z);
				transform.position = position;
			}
			if (lifetimeTimer > lifetime)
			{
				LeanTween.cancel(gameObject);
				Destroy(gameObject, lifetime);
			}
		}
		/// <summary>
		/// boxBehaviour is used to set the spawn location for the cube and make it possible to make the cube move towards the crane when clicked
		/// </summary>
		/// <param name="action"></param>
		/// <returns>Returns this boxbehaviour</returns>
		public BoxBehaviour Init(Action<Transform,BoxBehaviour> action, Transform Crane, PowerUpManager powerUpManager)
		{
			Vector3 position = new Vector3(Crane.position.x + UnityEngine.Random.Range(adjustmentLeftFromPlayer, adjustmentRightFromPlayer), Crane.position.y - 4, Crane.position.z + DISTANCE_FROM_CRANE);
			transform.position = position;
			this.Crane = Crane;
			Click += action;

			this.powerUpManager = powerUpManager;

			AddBobbing();

			return this;
		}
		/// <summary>
		/// OnMouseDown is used here to start the Click action to bring the cube to the crane by calling both the action and the returnTransform function
		/// </summary>
		private void OnMouseDown()
		{
			Click.Invoke(transform, this);
			ReturnTransform();
		}
		/// <summary>
		/// ReturnTransform does what it says, it returns the cube's transform once it's clicked 
		/// </summary>
		/// <returns>Returns the transform of this box</returns>
		private Vector3 ReturnTransform() => transform.position;
		/// <summary>
		/// Ontrigger enter is used here to make it so once the cube touches the crane it destroys it so it doesn't continue floating about
		/// </Summary>
		void OnTriggerEnter(Collider coll)
		{
			ScoreSystem.Instance().UpdateCrateScore();
			LeanTween.cancel(gameObject);
			powerUpManager.PickRandomPowerUp(type);
			Destroy(gameObject);
		}

		/// <summary>
		/// Stops the crate dead in it's tracks
		/// </summary>
		public void StopCrate()
		{
			LeanTween.moveZ(gameObject, transform.position.z - 4, 1f)
				.setEase(LeanTweenType.easeOutSine);
			shouldMove = false;
		}

		/// <summary>
		/// Enables the bobbing effect
		/// </summary>
		private void AddBobbing()
		{
			LeanTween.rotateZ(gameObject, -15, 2.5f)
				.setOnComplete(() =>
				{
					LeanTween.rotateZ(gameObject, 15, 2.5f).setEase(LeanTweenType.easeInOutQuad);
				}).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.delayedCall(5f, () => {
				LeanTween.rotateZ(gameObject, -15, 2.5f)
					.setOnComplete(() =>
					{
						LeanTween.rotateZ(gameObject, 15, 2.5f).setEase(LeanTweenType.easeInOutQuad);
					}).setEase(LeanTweenType.easeInOutQuad);
			}).setRepeat(-1);

			LeanTween.rotateX(gameObject, -10, 5f)
				.setOnComplete(() =>
				{
					LeanTween.rotateX(gameObject, 10, 5f).setEase(LeanTweenType.easeInOutQuad);
				}).setEase(LeanTweenType.easeInOutQuad);

			LeanTween.delayedCall(10f, () => {
				LeanTween.rotateX(gameObject, -5, 5f)
					.setOnComplete(() =>
					{
						LeanTween.rotateX(gameObject, 5, 5f)
						.setEase(LeanTweenType.easeInOutQuad);
					}).setEase(LeanTweenType.easeInOutQuad);
			}).setRepeat(-1);
		}
	}
}

