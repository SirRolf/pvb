namespace PvB.KM.Upgrades
{
	/// <summary>
	/// Marks for every department
	/// </summary>
	public enum MarkTypes
	{
		Mk1,
		Mk2,
		Mk3
	}
}