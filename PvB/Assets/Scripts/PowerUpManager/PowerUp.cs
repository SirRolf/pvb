using UnityEngine;

namespace PvB.KM.Powerups
{
	/// <summary>
	/// Interface for the powerups
	/// </summary>
	public abstract class PowerUp : MonoBehaviour
	{
		/// <summary>
		/// Effect of this powerup
		/// </summary>
		public abstract void Effect();
	}
}
