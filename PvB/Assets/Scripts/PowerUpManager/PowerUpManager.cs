using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Powerups
{
    /// <summary>
    /// Manager for starting the correct power ups
    /// </summary>
    public class PowerUpManager : MonoBehaviour
    {
        /// <summary>
        /// list of all the powerups for the big boxes
        /// </summary>
        [SerializeField]
        private List<PowerUp> BigBoxPowerUps;
        /// <summary>
        /// list of all the powerups for the small boxes
        /// </summary>
        [SerializeField]
        private List<PowerUp> SmallBoxPowerUps;
        /// <summary>
        /// list of all the powerups for the chests
        /// </summary>
        [SerializeField]
        private List<PowerUp> ChestPowerUps;

        /// <summary>
        /// Activates a random powerup depending on the chest you grabbed
        /// </summary>
        /// <param name="type">what chest you should get a powerup from</param>
        public void PickRandomPowerUp(ChestTypes type)
        {
            switch (type)
            {
                case ChestTypes.BigBox:
                    BigBoxPowerUps[Random.Range(0, BigBoxPowerUps.Count)].Effect();
                    break;
                case ChestTypes.SmallBox:
                    SmallBoxPowerUps[Random.Range(0, SmallBoxPowerUps.Count)].Effect();
                    break;
                case ChestTypes.Chest:
                    ChestPowerUps[Random.Range(0, ChestPowerUps.Count)].Effect();
                    break;
                default:
                    break;
            }
        }
    }
}
