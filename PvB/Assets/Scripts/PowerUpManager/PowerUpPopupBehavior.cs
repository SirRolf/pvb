using PvB.KM.DepartmentManagment;
using PvB.KM.Upgrades;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.KM.Powerups
{
	/// <summary>
	/// Class for the upgrade animations
	/// </summary>
	public class PowerUpPopupBehavior : MonoBehaviour
	{
		/// <summary>
		/// The canvas group of the black bar
		/// </summary>
		[SerializeField]
		private CanvasGroup blackCanvasGroup;
		/// <summary>
		/// Canvas group of the grey part that moves
		/// </summary>
		[SerializeField]
		private CanvasGroup whiteCanvasGroup;
		/// <summary>
		/// The black bar that fades
		/// </summary>
		[SerializeField]
		private Image blackUnderLayer;
		/// <summary>
		/// background is the moving grey part
		/// </summary>
		[SerializeField]
		private Image background;
		/// <summary>
		/// Icon background for Mark 2 upgrades
		/// </summary>
		[SerializeField]
		private Image iconBackgroundMk2;
		/// <summary>
		/// Icon background for Mark 3 upgrades
		/// </summary>
		[SerializeField]
		private Image iconBackgroundMk3;
		/// <summary>
		/// Icon for Mark 2
		/// </summary>
		[SerializeField]
		private Image iconMk2;
		/// <summary>
		/// Icon for Mark 3
		/// </summary>
		[SerializeField]
		private Image iconMk3;
		/// <summary>
		/// The text that floats in for Mark 2
		/// </summary>
		[SerializeField]
		private Image textMk2;
		/// <summary>
		/// The text that floats in for Mark 3
		/// </summary>
		[SerializeField]
		private Image textMk3;

		[Header("Department sprites")]
		/// <summary>
		/// Sprite for Mark 2 cannon
		/// </summary>
		[SerializeField]
		private Sprite mk2Cannon;
		/// <summary>
		/// Sprite for Mark 3 cannon
		/// </summary>
		[SerializeField]
		private Sprite mk3Cannon;
		/// <summary>
		/// Sprite for Mark 2 machine gun
		/// </summary>
		[SerializeField]
		private Sprite mk2MachineGun;
		/// <summary>
		/// Sprite for Mark 3 machine gun
		/// </summary>
		[SerializeField]
		private Sprite mk3MachineGun;
		/// <summary>
		/// Sprite for Mark 2 watercannon
		/// </summary>
		[SerializeField]
		private Sprite mk2WaterCannon;
		/// <summary>
		/// Sprite for Mark 3 watercannon
		/// </summary>
		[SerializeField]
		private Sprite mk3WaterCannon;
		/// <summary>
		/// Sprite for Mark 2 radar
		/// </summary>
		[SerializeField]
		private Sprite mk2Radar;
		/// <summary>
		/// Sprite for Mark 3 crane
		/// </summary>
		[SerializeField]
		private Sprite mk2Crane;

		/// <summary>
		/// Purple color for Mark 2 upgrades
		/// </summary>
		[SerializeField]
		private Color32 purple;
		/// <summary>
		/// Red color for Mark 3 upgrades
		/// </summary>
		[SerializeField]
		private Color32 red;

		/// <summary>
		/// Plays upgrade animation
		/// </summary>
		/// <param name="type">Department</param>
		/// <param name="mk">Upgrade Mark</param>
		public void PlayMarkUpgrade(DepartmentType type, MarkTypes mk)// using int for now but we should use the enum brandon made
		{
			background.rectTransform.localPosition = new Vector3(-blackUnderLayer.rectTransform.rect.width, background.rectTransform.localPosition.y);

			// Switch case for the upgrade Mark
			if (mk == MarkTypes.Mk2)
			{
				switch (type)
				{
					case DepartmentType.Cannon:
						iconMk2.sprite = mk2Cannon;
						break;
					case DepartmentType.MachineGun:
						iconMk2.sprite = mk2MachineGun;
						break;
					case DepartmentType.WaterCannon:
						iconMk2.sprite = mk2WaterCannon;
						break;
					case DepartmentType.Radar:
						iconMk2.sprite = mk2Radar;
						break;
					case DepartmentType.Krane:
						iconMk2.sprite = mk2Crane;
						break;
					default:
						break;
				}

				// Standard start position
				textMk2.rectTransform.localPosition = new Vector3(0, -Screen.height);
				iconBackgroundMk2.rectTransform.localPosition = new Vector3(Screen.width * 1.5f, iconBackgroundMk2.rectTransform.localPosition.y);
				background.color = purple;

				// Text floats in from under
				textMk2.enabled = true;
				LeanTween.moveY(textMk2.rectTransform, -Screen.height, 0.5f)
					.setOnComplete(() =>
					{
						LeanTween.moveY(textMk2.rectTransform, 0, 1.25f)
							.setEase(LeanTweenType.easeOutQuart)
							.setOnComplete(() =>
							{
								// If I have time to spare, I'll try to use Coroutine here.
								LeanTween.moveY(textMk2.rectTransform, 0, 1f)
									.setOnComplete(() =>
									{
										LeanTween.moveY(textMk2.rectTransform, -Screen.height, 1.25f)
											.setEase(LeanTweenType.easeInOutQuart)
											.setOnComplete(() => textMk2.enabled = false);
									});
							});
					});

				// Animation for popping or swiping the entire the entire icon
				iconMk2.enabled = true;
				iconBackgroundMk2.enabled = true;
				LeanTween.moveX(iconBackgroundMk2.rectTransform, Screen.width * 1.25f, 0.4f)
					.setOnComplete(() =>
					{
						LeanTween.moveX(iconBackgroundMk2.rectTransform, iconBackgroundMk2.rectTransform.rect.width * .5f, 0.9f) //0.4
							.setOnComplete(() =>
							{
								LeanTween.moveX(iconBackgroundMk2.rectTransform, -iconBackgroundMk2.rectTransform.rect.width * .5f, 1.2f) //3.2
									.setOnComplete(() =>
									{
										LeanTween.moveX(iconBackgroundMk2.rectTransform, -Screen.width * 1.125f, 0.9f)
											.setOnComplete(() =>
											{
												iconMk2.enabled = false;
												iconBackgroundMk2.enabled = false;
											});
									});
							});
					});
			}

			else if (mk == MarkTypes.Mk3)
			{
				switch (type)
				{
					case DepartmentType.Cannon:
						iconMk3.sprite = mk3Cannon;
						break;
					case DepartmentType.MachineGun:
						iconMk3.sprite = mk3MachineGun;
						break;
					case DepartmentType.WaterCannon:
						iconMk3.sprite = mk3WaterCannon;
						break;
					case DepartmentType.Radar:
						iconMk3.sprite = mk2Radar;
						break;
					case DepartmentType.Krane:
						iconMk3.sprite = mk2Crane;
						break;
					default:
						break;
				}

				// Standard position
				textMk3.rectTransform.localPosition = new Vector3(0, -Screen.height);
				iconBackgroundMk3.rectTransform.localPosition = new Vector3(Screen.width * 1.5f, -(iconBackgroundMk3.rectTransform.rect.height * .30f)); //For swipe in from the right
				background.color = red;

				// Text floats in from under
				textMk3.enabled = true;
				LeanTween.moveY(textMk3.rectTransform, -(textMk3.rectTransform.rect.height), 0.5f)
					.setOnComplete(() =>
					{
						LeanTween.moveY(textMk3.rectTransform, 0, 1.25f)
							.setEase(LeanTweenType.easeOutQuart)
							.setOnComplete(() =>
							{
								// If I have time to spare, I'll try to use Coroutine here.
								LeanTween.moveY(textMk3.rectTransform, 0, 1f)
									.setOnComplete(() =>
									{
										LeanTween.moveY(textMk3.rectTransform, -Screen.height, 1.25f)
											.setEase(LeanTweenType.easeInOutQuart)
											.setOnComplete(() => textMk3.enabled = false);
									});
							});
					});

				// Animation for popping or swiping the entire the entire icon
				iconMk3.enabled = true;
				iconBackgroundMk3.enabled = true;
				LeanTween.moveX(iconBackgroundMk3.rectTransform, Screen.width * 1.25f, 0.4f)
					.setOnComplete(() =>
					{
						LeanTween.moveX(iconBackgroundMk3.rectTransform, iconBackgroundMk3.rectTransform.rect.width * .5f, 0.9f) //0.4
							.setOnComplete(() =>
							{
								LeanTween.moveX(iconBackgroundMk3.rectTransform, -iconBackgroundMk3.rectTransform.rect.width * .5f, 1.2f) //3.2
									.setOnComplete(() =>
									{
										LeanTween.moveX(iconBackgroundMk3.rectTransform, -Screen.width * 1.125f, 0.9f) //0.4
											.setOnComplete(() =>
											{
												iconMk3.enabled = false;
												iconBackgroundMk3.enabled = false;
											});
									});
							});
					});
			}

			// Alpha for black bar
			LeanTween.alphaCanvas(blackCanvasGroup, 1, 0.5f)
				.setOnComplete(() =>
				{
					// If I have time to spare, I'll try to use Coroutine here.
					LeanTween.alphaCanvas(blackCanvasGroup, 1, 2.5f)
						.setOnComplete(() =>
						{
							LeanTween.alphaCanvas(blackCanvasGroup, 0, 0.5f);
						});
				});

			// Animation for white overlayer
			background.enabled = true;
			LeanTween.moveX(background.rectTransform, -blackUnderLayer.rectTransform.rect.width, 0.4f)
				.setOnComplete(() =>
				{
					LeanTween.moveX(background.rectTransform, -blackUnderLayer.rectTransform.rect.width * .125f, 0.9f)
						.setOnComplete(() =>
						{
							LeanTween.moveX(background.rectTransform, blackUnderLayer.rectTransform.rect.width * .125f, 1.2f)
								.setOnComplete(() =>
								{
									LeanTween.moveX(background.rectTransform, blackUnderLayer.rectTransform.rect.width, 0.9f)
										.setOnComplete(() => background.enabled = false);
								});
						});
				});
		}
	}
}
