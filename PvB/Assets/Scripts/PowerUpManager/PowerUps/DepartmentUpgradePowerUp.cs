using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PvB.KM.CrewManager.DepartmentManagment;
using PvB.KM.Util;
using PvB.KM.Upgrades;
using PvB.KM.Powerups;

namespace PvB.KM.Powerups
{
	/// <summary>
	/// Class for upgrading departments and calling the upgrade animation function
	/// </summary>
	public class DepartmentUpgradePowerUp : PowerUp
	{
		/// <summary>
		/// List to add the departments to.
		/// </summary>
		[SerializeField]
		private List<Department> departments;
		/// <summary>
		/// Script that plays the upgrade graphic.
		/// </summary>
		[SerializeField]
		private PowerUpPopupBehavior graphic;
		/// <summary>
		/// Dictionary for storing the department and mark enums.
		/// </summary>
		private Dictionary<Department, MarkTypes> DepartmentMarkList = new Dictionary<Department, MarkTypes>();

		/// <summary>
		/// Randomly gets a department, and then upgrades it.
		/// </summary>
		public override void Effect()
		{
			Department department = departments[UnityEngine.Random.Range(0, departments.Count)];
			MarkTypes mark = (MarkTypes)((int)DepartmentMarkList[department]);
			if (mark != MarkTypes.Mk3)
				mark++;
			DepartmentMarkList[department] = mark;
			department.Upgrade();
			graphic.PlayMarkUpgrade(department.departmentType, mark);
		}

		/// <summary>
		/// Adds both departments and mark to the dictionary.
		/// </summary>
		private void OnEnable()
		{
			for (int i = 0; i < departments.Count; i++)
				DepartmentMarkList.Add(departments[i], MarkTypes.Mk1);
		}
	}
}