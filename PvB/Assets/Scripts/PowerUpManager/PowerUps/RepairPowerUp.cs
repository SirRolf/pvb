using PvB.KM.PlayerHandling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Powerups
{
    /// <summary>
    /// Powerup for repairing the player
    /// </summary>
    public class RepairPowerUp : PowerUp
    {
        /// <summary>
        /// The player
        /// </summary>
        [SerializeField]
        private PlayerHandler player;
        /// <summary>
        /// Amount to heal the player
        /// </summary>
        [SerializeField]
        private float healingAmount;

        /// <summary>
        /// <inheritdoc/>
        /// </summary>
        public override void Effect() => player.UpdateHealth(healingAmount);
    }
}
