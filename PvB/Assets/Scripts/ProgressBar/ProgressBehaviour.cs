using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.ProgressBar
{
    public class ProgressBehaviour : MonoBehaviour
    {

        /// <summary>
        /// Sets the bar that shows the progress
        /// </summary>
        [SerializeField]
        private RectTransform Progress;
        ///<summary>
        /// Takes the waves from the waveManegemant script
        ///</summary>
        [SerializeField]
        private WaveManegemant WaveBar;
        /// <summary>
        /// When enabled takes account of the wavemanager whenever a wave is complete and adds a point to the bar
        /// </summary>
        private void OnEnable() => WaveBar.WaveCompleted += UpdateProgress;
        
        /// <summary>
        /// Moves the progress bar
        /// </summary>
        public void UpdateProgress(float startBar, float CurrentBar)
        {
            LeanTween.cancel(gameObject);
            Vector2 maxAnchor = Progress.anchorMax;
            LeanTween.value(gameObject, maxAnchor.y, CurrentBar / startBar, .5f)
                .setEase(LeanTweenType.easeOutQuart)
                .setOnUpdate((float amount) => Progress.anchorMax = new Vector2(1f, amount));
        }
    }
}
