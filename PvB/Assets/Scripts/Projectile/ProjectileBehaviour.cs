using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Projectile
{
    /// <summary>
    /// This class is used to set the projectiles behaviour 
    /// </summary>
    public class ProjectileBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Sets the distance that the projectile is allowed to travel
        /// </summary>
        private const float EFFECTIVE_RANGE = 50f;
        /// <summary>
        /// Velocity is used to set the speed the projectile goes
        /// </summary>
        [SerializeField]
        private float velocity;
        /// <summary>
        /// How much the speed gets reduced per second
        /// </summary>
        [SerializeField]
        private float speedReduction;
        /// <summary>
        /// Sets the lifetime of the projectile till it gets destroyed
        /// </summary>
        [SerializeField]
        private float lifetime;
		/// <summary>
        /// This is used to set the projectile object 
        /// </summary>
        [SerializeField]
        private GameObject Projectile;
        /// <summary>
        /// ImpactFX is used to set the effect for when the projectile hits an enemy or the player
        /// </summary>
        [SerializeField]
        private GameObject ImpactFX;
        /// <summary>
        /// Damage is used to set the damage for the projectile
        /// </summary>
        public float damage;

        /// <summary>
        /// The update here is temporary for the movement untill we can polish it with leantween
        /// </summary>
        private void Update()
        {
            Vector3 position = transform.position + transform.forward * velocity * Time.deltaTime;
            transform.position = position;
            velocity -= speedReduction * Time.deltaTime;
        }
        /// <summary>
        /// OnEnable is used here to set the lifetime of the projectile 
        /// </summary>
        private void OnEnable()
        {
            Destroy(Projectile, lifetime);
        }
        ///<summary>
        /// init gets called when the projectile is istatiated and makes it move forward
        ///</summary>
		/*private void Init()
        {
            LeanTween.moveZ(Projectile, EFFECTIVE_RANGE, velocity)
            .setOnComplete(() => Destroy(Projectile))
           	.setEase(LeanTweenType.easeOutQuint);
        }*/ ///commented for later use when polishing

        /// <summary>
        /// When this get called it calls the ImpactFFx function and executes it
        /// </summary>
        public void DisableProjectile()
        {
            ImpactFFX();
            Destroy(Projectile);
        }
        /// <summary>
        /// Instantiates the Impact effect when it gets called upon it also destroys the projectile after hitting
        /// </summary>
        private void ImpactFFX()
        {
                Instantiate(ImpactFX, transform.position, transform.rotation * Quaternion.Euler(90f, 0f, 0f));
        }
    }
}