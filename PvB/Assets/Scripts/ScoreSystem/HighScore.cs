using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.IO;

namespace PvB.KM.ScoreSystems
{
	///<summary>
	/// This class is for the highSchore system
	///</summary>
	public class HighScore : MonoBehaviour
	{
		///<summary>
		/// the filepath towards the XML file
		///</summary>
		private const string FILENAME = "/HighScore/HighScore.xml";
		///<summary>
		/// a private text to put in the 1st place score box
		///</summary>
		[SerializeField]
		private Text score1;
		///<summary>
		/// a private text to put in the 2nd place score box
		///</summary>
		[SerializeField]
		private Text score2;
		///<summary>
		/// a private text to put in the 3rd place score box
		///</summary>
		[SerializeField]
		private Text score3;

		///<summary>
		/// When enabled this wil load the scores into a string and display it in a text box in the main menu
		///</summary>
		private void OnEnable()
		{
			score1.text = LoadScoreList()[0].ToString();
			score2.text = LoadScoreList()[1].ToString();
			score3.text = LoadScoreList()[2].ToString();
		}
		/// <summary>
		/// Writes the Score to an XML file once obtained 
		/// </summary>
		/// <param name="highScore">Score amount</param>
		public static void SaveScore(int highScore)
		{
			List<int> SavedScore = LoadScoreList();
			SavedScore.Add(highScore);
			int[] Save = new int[SavedScore.Count];
			for (int i = 0; i < SavedScore.Count; i++)
				Save[i] = SavedScore[i];
			XmlSerializer serializer = new XmlSerializer(typeof(int[]));
			string filepath = Application.streamingAssetsPath + FILENAME;
			using (FileStream fileStream = new FileStream(filepath, FileMode.Create))
			{
				serializer.Serialize(fileStream, Save);
				fileStream.Close();
			}
		}

		/// <summary>
		/// Loads the score from an xml and gives a base score as placeholder on first startup
		/// </summary>
		/// <returns>finalScore returns the total score obtained at the end of the game</returns>
		public static List<int> LoadScoreList()
		{
			List<int> finalScore = new List<int>();

			XmlSerializer serializer = new XmlSerializer(typeof(int[]));
			string filepath = Application.streamingAssetsPath + FILENAME;
			using (FileStream fileStream = new FileStream(filepath, FileMode.Open))
			{
				int[] highScores = serializer.Deserialize(fileStream) as int[];
				for (int i = 0; i < highScores.Length; i++)
					finalScore.Add(highScores[i]);
				finalScore.Sort(SortScore);
				fileStream.Close();
				return finalScore;
			}
		}
		///<summary>
		/// Sorts the score once obtained
		///</summary>
		/// <returns>Returns the int to sort the highScore</returns>
		private static int SortScore(int a, int b)
		{
			if (b > a)
				return 1;
			if (a > b)
				return -1;
			return 0;
		}
	}
}