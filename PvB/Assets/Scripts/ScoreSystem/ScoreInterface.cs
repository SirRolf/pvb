using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace PvB.KM.ScoreSystems
{
    public class ScoreInterface : MonoBehaviour
    {
        ///<summary>
        /// Sets the text in which the total score will be displayed
        ///</summary>
        [SerializeField]
        private Text totalScore;
        ///<summary>
        /// Sets the text in which the kill count will be displayed
        ///</summary>
        [SerializeField]
        private Text killCount;
        ///<summary>
        /// sets the text in which the crateCount will be displayed
        ///</summary>
        [SerializeField]
        private Text crateCount;

        ///<summary>
        /// When enabled this will display the score on the victory screen
        ///</summary>
        private void OnEnable()
        {
            totalScore.text = ScoreSystem.Instance().GetScore().ToString();
            killCount.text = ScoreSystem.Instance().GetJetskis().ToString();
            crateCount.text = ScoreSystem.Instance().GetCrates().ToString();
        }
    }
}
