using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PvB.KM.ScoreSystems
{
    public class ScoreSystem : MonoBehaviour
    {
        ///<summary>
        /// Makes it possible to call the instance in other scripts
        ///</summary>
        public static ScoreSystem Instance() => instance;
        ///<summary>
        /// References this instance of the score system
        ///</summary>
        private static ScoreSystem instance;
        ///<summary>
        /// Sets the variable to be used for calculating the score
        ///</summary>
        [SerializeField]
        private float time;
        ///<summary>
        /// Sets the float for the jetskis to be used in GetScore
        ///</summary>
        [SerializeField]
        private float jetskis;
        ///<summary>
        /// Makes it possible to set the scores from the jetski's
        ///</summary>
        [SerializeField]
        private float jetskisScore;
        ///<summary>
        /// Sets the float for the crates to be used in GetScore
        ///</summary>
        [SerializeField]
        private float crates;
        ///<summary>
        /// Makes it possible to set the score you get from the crates
        ///</summary>
        [SerializeField]
        private float cratesScore;
        ///<summary>
        /// calls the instance in this script
        ///</summary>
        private void OnEnable() => instance = this;

        ///<summary>
        /// Gets the score from the crates and jetskis and divides it by the time it took to finish the game
        ///</summary>
        public float GetScore()
        {
            float score = 0;
            score = jetskis + crates / time;
            score = Mathf.Round(score);
            HighScore.SaveScore((int)score);
            return score;
        }
        ///<summary>
        /// Gets the jetskis variable for use in a different script
        ///</summary>
        public float GetJetskis() => jetskis;
        ///<summary>
        /// Gets the crates variable for use in a different script
        ///</summary>
        public float GetCrates() => crates;

        ///<summary>
        /// Keeps track of the time which than can be used to divide the score
        ///</summary>
        private void Update() => time += Time.deltaTime;

        ///<summary>
        /// Takes the score from jetskisScore and makes it useable in GetScore
        ///</summary>
        public void UpdateJetskiScore() => jetskis += jetskisScore;

        ///<summary>
        /// Takes the score from cratesScore and makes it useable in getScore
        ///</summary>
        public void UpdateCrateScore() => crates += cratesScore;
    }
}