using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Brandon Ruigrok
namespace PvB.KM.Sound.Shooting
{
	public class ShootingSound : MonoBehaviour
	{
		/// <summary>
		/// Sound effect location
		/// </summary>
		[SerializeField]
		private string fileLocation;
		/// <summary>
		/// Sound effect location for single audio
		/// </summary>
		[SerializeField]
		private string oneShotFileLocation;
		FMOD.Studio.EventInstance audioEvent;

		/// <summary>
		/// Plays the sound effect related to this object
		/// </summary>
		public void PlaySound()
		{
			audioEvent = FMODUnity.RuntimeManager.CreateInstance(fileLocation);
			audioEvent.start();
		}

		/// <summary>
		/// Stops the current sound effect related to this object.
		/// </summary>
		public void StopSound()
		{
			audioEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
		}

		/// <summary>
		/// Plays a sound only once.
		/// </summary>
		public void PlayOneShotSound()
		{
			FMODUnity.RuntimeManager.PlayOneShot(oneShotFileLocation);
		}

		/// <summary>
		/// Saves resources on disable.
		/// </summary>
		public void onDisable()
		{
			audioEvent.release();
		}
	}
}