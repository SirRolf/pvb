using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

//Brandon Ruigrok
namespace PvB.KM.Sound.Theme
{
	public class GameTheme : MonoBehaviour
	{

		/// <summary>
		/// Sound effect location
		/// </summary>
		[SerializeField]
		private string fileLocation;
		FMOD.Studio.EventInstance audioEvent;

		/// <summary>
		/// 0 is Main Menu, 1 is Tense, 2 is Rock, 3 is Victory, 4 is Idle
		/// </summary>
		private int main = 0, tense = 1, rock = 2, vic = 3;
		
		public void changeTheme(MusicTypes types)
		{
			switch (types)
			{
				case MusicTypes.mainTheme:
					toMain();
					break;
				case MusicTypes.gameTheme:
					toGame();
					break;
				case MusicTypes.gameOverTheme:
					toTense();
					break;
				case MusicTypes.vicTheme:
					toVic();
					break;
				case MusicTypes.soundDeletion:
					toStop();
					break;
			}
		}

		// Start is called before the first frame update
		private void Start()
		{
			audioEvent = RuntimeManager.CreateInstance(fileLocation);
			toMain();
			audioEvent.start();
		}

		/// <summary>
		/// To Start sound.
		/// </summary>
		private void toMain()
		{
			audioEvent.setParameterByName("Change Music (0-4)", main, true);
		}

		/// <summary>
		/// To tutorial / game over.
		/// </summary>
		private void toTense()
		{
			audioEvent.setParameterByName("Change Music (0-4)", tense, true);
		}

		/// <summary>
		/// To game music.
		/// </summary>
		private void toGame()
		{
			audioEvent.setParameterByName("Change Music (0-4)", rock, true);
		}

		/// <summary>
		/// Victory music.
		/// </summary>
		private void toVic()
		{
			audioEvent.setParameterByName("Change Music (0-4)", vic, true);
		}

		private void toStop()
		{
			audioEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
			audioEvent.release();
		}
	}
}