using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PvB.KM.Sound.Theme
{
	public enum MusicTypes
	{
		mainTheme,
		gameTheme,
		gameOverTheme,
		vicTheme,
		soundDeletion
	}
}