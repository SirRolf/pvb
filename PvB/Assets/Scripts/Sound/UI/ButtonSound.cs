using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Brandon Ruigrok
namespace PvB.KM.Sound.UI
{
	public static class ButtonSound
	{
		/// <summary>
		/// Location for the sound effect.
		/// </summary>
		private const string BUTTON_EVENT = "event:/SFX One-Shots/UI Click Sound";

		/// <summary>
		/// Plays button sound.
		/// </summary>
		public static void playSound() => FMODUnity.RuntimeManager.PlayOneShot(BUTTON_EVENT);
	}
}