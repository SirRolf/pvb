using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PvB.KM.MainMenu;
using PvB.KM.Sound.UI;
using PvB.KM.CustomCamera;
using PvB.KM.DepartmentManagment;
using PvB.KM.Pause;
using PvB.KM.Sound.Theme;
using PvB.KM.CrewManagment;

//Brandon Ruigrok
namespace PvB.KM.Start
{
	/// <summary>
	/// Handles the startup sequence of the game.
	/// </summary>
	public class StartManager : MonoBehaviour
	{
#if UNITY_EDITOR
		private const bool DEBUG_SKIP_TUTORIAL = true;
#else
		private const bool DEBUG_SKIP_TUTORIAL = false;
#endif

		/// <summary>
		/// The Main camerabehavior
		/// </summary>
		[SerializeField]
		private CameraBehavior camera;
		/// <summary>
		/// Fades the main menu away.
		/// </summary>
		[SerializeField]
		private MainMenuBehavior menuBehavior;
		/// <summary>
		/// The manager controlling all the departments. just for the StartDepartments function
		/// </summary>
		[SerializeField]
		private DepartmentManager departmentManager;
		/// <summary>
		/// The manager controlling the waves of the enemies. This is here just for the StartWaves function
		/// </summary>
		[SerializeField]
		private WaveManegemant waveManager;
		/// <summary>
		/// The hud for the main game
		/// </summary>
		[SerializeField]
		private CanvasGroup gameHud;
		/// <summary>
		/// The pause manager just so the pause menu can only show up in game
		/// </summary>
		[SerializeField]
		private PauseManager pauseManager;
		/// <summary>
		/// The manager for showing the player around
		/// </summary>
		[SerializeField]
		private TutorialManager tutorialManager;
		/// <summary>
		/// Soundtrack of the game.
		/// </summary>
		[SerializeField]
		private GameTheme music;
		/// <summary>
		/// Initialises the game until full boot up.
		/// </summary>
		public void StartUp()
		{
			music.changeTheme(MusicTypes.gameTheme);
			CrewMemberManager.ClearAllDepartments();
			menuBehavior.HideMain();
			camera.ResetCameraPosition(2f);
			camera.ResetCameraRotation(2f);
			LeanTween.alphaCanvas(gameHud, 1, 2f)
				.setEase(LeanTweenType.easeOutQuad)
				.setOnComplete(() =>
				{
					if (!DEBUG_SKIP_TUTORIAL)
					{
						tutorialManager.PlayTutorial(() =>
						{
							departmentManager.StartDepartments();
							waveManager.StartWaves();
							gameHud.interactable = true;
							gameHud.blocksRaycasts = true;
							pauseManager.CanBeActivated = true;
						});
					}
					else
					{
						departmentManager.StartDepartments();
						waveManager.StartWaves();
						gameHud.interactable = true;
						gameHud.blocksRaycasts = true;
						pauseManager.CanBeActivated = true;
					}
				});
		}
		
		/// <summary>
		/// Shuts the game down.
		/// </summary>
		public void Shutdown()
		{
			ButtonSound.playSound();
			Application.Quit();
		}
	}
}
