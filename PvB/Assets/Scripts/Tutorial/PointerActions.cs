using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.DepartmentManagment.Pointer
{
	/// <summary>
	/// Actions the pointer can take
	/// </summary>
	public enum PointerActions
	{
		Move,
		LeftClick,
		RightClick,
		ShowPointer,
		HidePointer,
		Wait
	}
}
