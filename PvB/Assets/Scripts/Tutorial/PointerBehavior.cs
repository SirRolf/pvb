using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PvB.KM.DepartmentManagment.Pointer
{
	/// <summary>
	/// Behavior for the pointer used in the tutorial
	/// </summary>
	public class PointerBehavior : MonoBehaviour
	{
		/// <summary>
		/// The transform of this pointer
		/// </summary>
		[SerializeField]
		private RectTransform rectTransform;
		/// <summary>
		/// The canvas group in the pointer
		/// </summary>
		[SerializeField]
		private CanvasGroup pointerCanvasGroup;
		/// <summary>
		/// the transform of the leftclick image
		/// </summary>
		[SerializeField]
		private RectTransform LeftClickTransform;
		/// <summary>
		/// the transform of the rightclick image
		/// </summary>
		[SerializeField]
		private RectTransform RightClickTransform;

		/// <summary>
		/// Decides what action to take and calls that function
		/// </summary>
		/// <param name="action">What type of pointer action should be used</param>
		/// <param name="location">Optional location</param>
		/// <param name="optionalAction">Optional action</param>
		/// <param name="waitTime">How long the Wait command should take</param>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		public void ActionDecider(PointerActions action, Vector2 location, Action optionalAction, float waitTime, Action onComplete)
		{
			switch (action)
			{
				case PointerActions.Move:
					MovePointer(location, optionalAction, onComplete);
					break;
				case PointerActions.LeftClick:
					Click(LeftClickTransform, optionalAction, onComplete);
					break;
				case PointerActions.RightClick:
					Click(RightClickTransform, optionalAction, onComplete);
					break;
				case PointerActions.ShowPointer:
					ShowPointer(onComplete);
					break;
				case PointerActions.HidePointer:
					HidePointer(onComplete);
					break;
				case PointerActions.Wait:
					StartCoroutine(Wait(waitTime, onComplete));
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// Moves the pointer to a new location
		/// </summary>
		/// <param name="location">what location to move the pointer to</param>
		/// <param name="optionalAction">Optional Action if needed</param>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		private void MovePointer(Vector2 location, Action optionalAction, Action onComplete)
		{
			LeanTween.move(rectTransform, location, 1f)
				.setEase(LeanTweenType.easeInOutQuad)
				.setOnComplete(() =>
				{
					onComplete.Invoke();
					if (optionalAction != null)
						optionalAction.Invoke();
				});
		}
		/// <summary>
		/// Shows the pointer
		/// </summary>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		public void ShowPointer(Action onComplete)
		{
			rectTransform.position = new Vector2(Screen.width / 2, Screen.height / 1.5f);
			LeanTween.alphaCanvas(pointerCanvasGroup, 1f, 1f)
				.setOnComplete(onComplete);
		}
		/// <summary>
		/// Hides the pointer
		/// </summary>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		public void HidePointer(Action onComplete)
		{
			LeanTween.alphaCanvas(pointerCanvasGroup, 0f, 1f)
				.setOnComplete(onComplete);
		}
		/// <summary>
		/// Clicks with pointer. Concider using OptionalAction to call what to click on
		/// </summary>
		/// <param name="clickTransform">the transform of one of the two click collor transforms</param>
		/// <param name="optionalAction">Optional Action if needed</param>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		private void Click(RectTransform clickTransform, Action optionalAction, Action onComplete)
		{
			LeanTween.alpha(clickTransform, 1f, .15f)
				.setOnComplete(() =>
				{
					if (optionalAction != null)
						optionalAction.Invoke();
					LeanTween.alpha(clickTransform, 0f, .5f)
						.setOnComplete(onComplete);
				});
		}
		/// <summary>
		/// Waits for the given amount of seconds
		/// </summary>
		/// <param name="waitTime">How long the pointer should wait</param>
		/// <param name="onComplete">The Oncomplete called after every Action</param>
		/// <returns>the wait time</returns>
		private IEnumerator Wait(float waitTime, Action onComplete)
		{
			yield return new WaitForSeconds(waitTime);
			onComplete.Invoke();
		}
	}
}
