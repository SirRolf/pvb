using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.DepartmentManagment.Pointer
{
	/// <summary>
	/// A call, used for saving what kind of action you want the pointer to take
	/// </summary>
	public class PointerCall
	{
		/// <summary>
		/// What type of action you want to make
		/// </summary>
		private PointerActions pointerAction;
		/// <summary>
		/// What type of action you want to make
		/// </summary>
		public PointerActions PointerAction => pointerAction;
		/// <summary>
		/// What location to move to if the pointer needed to move
		/// </summary>
		private Vector2 location;
		/// <summary>
		/// What location to move to if the pointer needed to move
		/// </summary>
		public Vector2 Location => location;
		/// <summary>
		/// Optional Action, always null if nothing is given.
		/// </summary>
		private Action optionalAction;
		/// <summary>
		/// Optional Action, always null if nothing is given.
		/// </summary>
		public Action OptionalAction => optionalAction;
		/// <summary>
		/// How long to wait in the wait function 
		/// </summary>
		private float waitTime;
		/// <summary>
		/// How long to wait in the wait function 
		/// </summary>
		public float WaitTime => waitTime;

		/// <summary>
		/// Constructor for most actions
		/// </summary>
		/// <param name="pointerAction">What action to take</param>
		/// <param name="optionalAction">Optional action if needed</param>
		public PointerCall(PointerActions pointerAction, Action optionalAction = null)
		{
			if (pointerAction == PointerActions.Move)
			{
				Debug.LogError("Please add a Vector2 if you want to move to a new location");
				return;
			}
			if (pointerAction == PointerActions.Wait)
			{
				Debug.LogError("Please add a a wait time if you want to wait");
				return;
			}
			this.pointerAction = pointerAction;
			this.optionalAction = optionalAction;
		}
		/// <summary>
		/// Constructor exclusivly used for the move action
		/// </summary>
		/// <param name="location">What location to move to</param>
		/// <param name="optionalAction">Optional action if needed</param>
		public PointerCall(Vector2 location, Action optionalAction = null)
		{
			pointerAction = PointerActions.Move;
			this.location = location;
			this.optionalAction = optionalAction;
		}
		/// <summary>
		/// Constructor exclusivly used for waiting
		/// </summary>
		/// <param name="waitTime">how long the pointer should wait</param>
		/// <param name="optionalAction">Optional action if needed</param>
		public PointerCall(float waitTime, Action optionalAction = null)
		{
			pointerAction = PointerActions.Wait;
			this.waitTime = waitTime;
			this.optionalAction = optionalAction;
		}
	}
}
