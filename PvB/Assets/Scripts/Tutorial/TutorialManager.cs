using PvB.KM.CrewManager.DepartmentManagment.Departments;
using PvB.KM.CrewManagment;
using PvB.KM.DepartmentManagment.Pointer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PvB.KM.DepartmentManagment
{
	/// <summary>
	/// The manager for showing the player around
	/// </summary>
	public class TutorialManager : MonoBehaviour
	{
		/// <summary>
		/// List of all the calls the pointer should do
		/// </summary>
		public List<PointerCall> callBacklog = new List<PointerCall>();

		/// <summary>
		/// How far we are into the <see cref="callBacklog"/>
		/// </summary>
		private int index = 0;

		/// <summary>
		/// The pointer
		/// </summary>
		[SerializeField]
		private PointerBehavior pointer;
		/// <summary>
		/// Toggle for the cannon department
		/// </summary>
		[SerializeField]
		private CrewUIToggleBehavior cannonDepartmentToggle;
		/// <summary>
		/// Toggle for the Machine gun department
		/// </summary>
		[SerializeField]
		private CrewUIToggleBehavior MachineGunDepartmentToggle;

		/// <summary>
		/// Adds all the pointer calls
		/// </summary>
		private void Start()
		{
			Vector2 ScreenSize = new Vector2(Screen.width, Screen.height);

			callBacklog.Add(new PointerCall(PointerActions.ShowPointer));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.06f, ScreenSize.y * 0.66f)));
			callBacklog.Add(new PointerCall(PointerActions.RightClick,() => MachineGunDepartmentToggle.Click(PointerEventData.InputButton.Right)));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.06f, ScreenSize.y * 0.74f)));
			callBacklog.Add(new PointerCall(PointerActions.LeftClick, () => cannonDepartmentToggle.Click(PointerEventData.InputButton.Left)));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.22f, ScreenSize.y * 0.64f)));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.85f, ScreenSize.y * 0.64f)));
			callBacklog.Add(new PointerCall(1.5f));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.22f, ScreenSize.y * 0.64f)));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.06f, ScreenSize.y * 0.74f)));
			callBacklog.Add(new PointerCall(PointerActions.LeftClick, () => cannonDepartmentToggle.Click(PointerEventData.InputButton.Left)));
			callBacklog.Add(new PointerCall(new Vector2(ScreenSize.x * 0.06f, ScreenSize.y * 0.66f)));
			callBacklog.Add(new PointerCall(PointerActions.LeftClick, () => MachineGunDepartmentToggle.Click(PointerEventData.InputButton.Left)));
			callBacklog.Add(new PointerCall(PointerActions.HidePointer));
		}

		/// <summary>
		/// Starts the tutorial
		/// </summary>
		/// <param name="onComplete">Will get called when the tutorial is done</param>
		public void PlayTutorial(Action onComplete) => BacklogLoop(onComplete);

		/// <summary>
		/// The actual looping through all the actions
		/// </summary>
		/// <param name="onComplete">will be called if the whole loop is done</param>
		private void BacklogLoop(Action onComplete)
		{
			pointer.ActionDecider(callBacklog[index].PointerAction, callBacklog[index].Location, callBacklog[index].OptionalAction, callBacklog[index].WaitTime, () =>
			{
				index++;
				if (callBacklog.Count > index)
					BacklogLoop(onComplete);
				else
					onComplete.Invoke();
			});
		}
	}
}
