using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PvB.KM.UI
{
	/// <summary>
	/// Class for making toggles switch image when you toggle them
	/// </summary>
	public class ToggleImageChanger : MonoBehaviour
	{
		/// <summary>
		/// The toggle used for checking if you want to use the on or off image
		/// </summary>
		[SerializeField]
		private Toggle toggle;
		/// <summary>
		/// The image you want to change
		/// </summary>
		[SerializeField]
		private Image toggleImage;
		/// <summary>
		/// The sprite for when the toggle is off
		/// </summary>
		[SerializeField]
		private Sprite onImage;
		/// <summary>
		/// The sprite for when the toggle is on
		/// </summary>
		[SerializeField]
		private Sprite offImage;
		/// <summary>
		/// Action that will be assigned to the toggle for checking if the value is changed
		/// </summary>
		private UnityAction<bool> valueChanged;

		private void OnEnable()
		{
			valueChanged += OnValueChanged;
			toggle.onValueChanged.AddListener(valueChanged);
		}
		private void OnDisable()
		{
			toggle.onValueChanged.RemoveListener(valueChanged);
			valueChanged -= OnValueChanged;
		}

		/// <summary>
		/// Changes the sprite to the toggles given value
		/// </summary>
		/// <param name="value">if the toggle is on or off</param>
		private void OnValueChanged(bool value)
		{
			if (value)
				toggleImage.sprite = onImage;
			else
				toggleImage.sprite = offImage;
		}
	}
}
