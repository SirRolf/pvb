using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.Util
{
    /// <summary>
    /// Helper class used for colliders that are attached to another gameobject
    /// </summary>
    public class ColliderHelper : MonoBehaviour
    {
        /// <summary>
        /// Gets called whenever this object is collided with
        /// </summary>
        public Action<Collider> Collided;
        private void OnTriggerEnter(Collider collision) => Collided.Invoke(collision);
    }
}
