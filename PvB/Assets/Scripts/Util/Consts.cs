namespace PvB.KM.Util
{
	/// <summary>
	/// class ful of consts
	/// </summary>
	public class Consts
	{
		/// <summary>
		/// How many crew you have
		/// </summary>
		public const int CREW_MEMBER_AMOUNT = 2;
	}
}
