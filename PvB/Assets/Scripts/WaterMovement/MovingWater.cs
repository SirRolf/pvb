using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PvB.KM.WaterMovement
{
    /// <summary>
    /// moves the water tiles
    /// </summary>
    public class MovingWater : MonoBehaviour
    {
        /// <summary>
        /// the speed at which they move
        /// </summary>
        [SerializeField]
        private float SPEED;
        /// <summary>
        /// collection of tilecontaindersd that hold the water tiles
        /// </summary>
        [SerializeField]
        private List<GameObject> WaterTiles;
        /// <summary>
        /// moves all tiles forward * SPEED & moves the most backward tile to the front and sorts the de list
        /// </summary>
        private void Update()
        {
            int lastTile = WaterTiles.Count - 1;
            foreach (GameObject tile in WaterTiles)
                tile.transform.Translate(Vector3.back * SPEED * Time.deltaTime);

            if (WaterTiles[lastTile].transform.position.z < -200)
            {             
                WaterTiles[lastTile].transform.position = new Vector3(WaterTiles[0].transform.position.x, WaterTiles[0].transform.position.y, WaterTiles[0].transform.position.z + 200);
                WaterTiles.Sort(SortByZ);
            }
        }
        /// <summary>
        /// sort list on the z value of there posistion
        /// </summary>
        /// <param name="a">value in list a</param>
        /// <param name="b">value in list b</param>
        /// <returns></returns>
        private int SortByZ(GameObject a, GameObject b)
        {
            if (a.transform.position.z < b.transform.position.z)
                return 1;
            if (a.transform.position.z > b.transform.position.z)
                return -1;
            return 0;
        }
    }

}
