![Logo](PvB/Assets/Art/Logo/Logo_OceanShield.png)

# Operation Ocean Shield
Active tower defense game where the player is tasked with destroying enemy ships, while alternating both the crew of the boat, and the player between the different roles that are on the ship. The goal is to reach the end before your ship loses all of its hit points and sinks into the ocean.

We got tasked to do this by the Dutch Royal Navy.

# Work processes
We kicked the project off with discussing what programs and engine to use. Where we concluded that we would like to use the [Unity game engine](https://store.unity.com/#plans-individual), [bitbucket](https://bitbucket.org/SirRolf/pvb/src/develop/) for version control, and google drive for file sharing between all developers and artists. We also used [Trello](https://trello.com/b/KxbE4puj/proeve-van-bekwaamheid-team-06-2021) to keep up with the tasks, and see what everyone is doing.

# Team
This project was made in a team of 11 people, 4 devs, 4 artists and 3 sound designers.

Devs | Artists | Sound designers
-----| -------- | --------
Floris van den Berg (Lead dev). | Quincy Sung (Lead artist). | David Bock(Lead Sound design).
Bradley Bekker. | Calvin Buth. | Jannis Heidemann.
Tiësto Schouten. | Jaynie Beijer. | Marvin Losch.
Brandon Ruigrok. | Roan van Ligten. |

# Sources
In the project we've worked with multiple plugins.

* [FMOD by Firelight Technologies](https://www.fmod.com/)
* [LeanTween by Dented Pixel](https://assetstore.unity.com/packages/tools/animation/leantween-3595)
* [ShaderGraph by Unity](https://unity.com/shader-graph)

# Software
For the development of the project we've used a few different programs, all listed here.

* [Unity3D Engine 2020.2.4f1](https://store.unity.com/#plans-individual)
* [Trello](https://trello.com/b/KxbE4puj/proeve-van-bekwaamheid-team-06-2021)
* [Bitbucket](https://bitbucket.org/SirRolf/pvb/src/develop/)
* [Sourcetree](https://www.sourcetreeapp.com/)
* [Google Drive](https://www.google.com/drive/)

# license
[MIT License.](https://bitbucket.org/SirRolf/pvb/src/6814c3fbc315ba25ac31867af11ab189e5ae843d/LICENSE?at=feature%2FLicense)
